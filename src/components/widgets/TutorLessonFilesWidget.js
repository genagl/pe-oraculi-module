import React, { Component } from "react"
import { __ } from "react-pe-utilities"

class TutorLessonFilesWidget extends Component {
  render() {
    const dnld = [1, 2, 3, 4].map((e, i) => {
      if (this.props[`include_id${e}`] === "false") {
        return null
      }

      return (
        <a className="btn btn-secondary btn-lg m-1" href={this.props[`include_id${e}`]} download key={i}>
          <i className="far fa-file-archive" />
        </a>
      )
    })
    return (
      <div className="course_category_select">
        <div className="aside-widget-title">
          {__("Matherials for Lesson:")}
        </div>
        {dnld}
      </div>
    )
  }
}

export default TutorLessonFilesWidget
