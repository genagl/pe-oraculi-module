/*
правая панель на страницах курса, результатов поиска
 */
import React, { Component } from "react"
import { Query, withApollo } from "react-apollo"
import { compose } from "recompose"
import { withRouter } from "react-router"
import { __ } from "react-pe-utilities"
import {
  getQueryArgs, getQueryName, queryCollection,
} from "react-pe-layouts"
import {Loading} from 'react-pe-useful'

class CourseFilesWidget extends Component {
  constructor(props) {
    super(props)
    this.state = {
      
    }
  }

    rightPanel = (single, id) => {
      return (
        <>
          <div className="course_category_select">
            <div className="filter category">
              {__("Файлы курса:")}
            </div>
            <div className="btn btn-primary">
              {__("Скачать")}
            </div>
          </div>
        </>
      )
    }

    render() {
      const query_name = getQueryName("Bio_course")
      const query_args = getQueryArgs("Bio_course")

      const query = queryCollection("Bio_course", query_name, query_args)

      const { id } = this.props.match.params

      return (
        <Query query={query}>
          {
                ({
                  loading, error, data, client,
                }) => {
                  if (loading) {
                    return <Loading />
                  }
                  if (data) {
                    console.log(data)
                    const single = data[query_name][query_name][0] ? data[query_name][query_name][0] : []

                    return this.rightPanel(single, id)
                  }
                  if (error) {
                    return error.toString()
                  }
                }
            }
        </Query>
      )
    }
}
export default compose(
  withApollo,
  withRouter,
)(CourseFilesWidget)
