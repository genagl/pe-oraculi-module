import React, { Component, } from "react"
import { NavLink } from "react-router-dom"
import { __ } from "react-pe-utilities"

class CurrentCourseTranslation extends Component {
  render() {
    return (
      <NavLink className="btn btn-light" to="/translation/3290">
        {__("Translation")}
      </NavLink>
    )
  }
}
export default CurrentCourseTranslation
