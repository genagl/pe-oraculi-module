import React, { Component } from "react"
import { withRouter } from "react-router"
import { compose } from "recompose"
import { Mutation, withApollo } from "react-apollo"
import { Intent } from "@blueprintjs/core"
import gql from "graphql-tag"
import { AppToaster } from 'react-pe-useful'
import { __ } from "react-pe-utilities"

class TutorLikerWidget extends Component {
	constructor(props) {
		super(props)
		this.state = {
			is_favorite: this.props.is_favorite,
		}
	}

	render() {
		const mutation = gql`
		mutation changeBio_Article( $id:String, $input: Bio_ArticleInput)
		{
		  changeBio_Article( id:$id, input:$input )
		  {
			  is_favorite
		  }
		}
		`
		return (
			<Mutation mutation={mutation}>
				{
					(changeBio_Article, data) => (
						<div
							className={` like ${this.state.is_favorite ? " liked" : ""}`}
							title={__("add to Favorites")}
							onClick={(evt) => {
								evt.preventDefault()
								this.onLike(evt, changeBio_Article)
							}}
						/>
					)
				}
			</Mutation>
		)
	}

	onLike = (evt, changeBio_Article) => {
		changeBio_Article({
			variables:
			{
				id: this.props.id,
				input: {
					is_favorite: !this.state.is_favorite,
				},
			},
			update: (store, data) => {
				console.log(data.data.changeBio_Article)
				this.setState({ is_favorite: !this.state.is_favorite })
				if (data.data.changeBio_Article.is_favorite) {
					AppToaster.show({
						intent: Intent.SUCCESS,
						icon: "tick",
						message: __("Добавлено в избранное"),
					})
				} else {
					AppToaster.show({
						intent: Intent.WARNING,
						icon: "tick",
						message: __("Удалено изи избранного"),
					})
				}
			},
		})
		// ;
	}
}

export default compose(
	withApollo,
	withRouter,
)(TutorLikerWidget)
