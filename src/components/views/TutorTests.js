import React from "react"
import { withRouter } from "react-router"
import { compose } from "recompose"
import { withApollo } from "react-apollo"
import { __ } from "react-pe-utilities"
import BasicState from "react-pe-basic-view"
import TestQuote from "./tutor/TestQuote"
//import Feed from "../../../layouts/BasicState/Feed"
import { Feed } from "react-pe-basic-view"
import { initArea } from "react-pe-utilities" 

class TutorTests extends BasicState {
  getRoute = () => "all-tests"

  addRender() {
    const { id, articles } = this.props
    const data_type = "Bio_Test"
    const paging = ""
    return (
      <div className=" mt-5">
        <div className="tutor-row">
          <div className="tutor-left-aside-2  mobile-relatived">
            {
              initArea(
                "tutor-tests-left-aside",
                { ...this.props },
              )
            }
          </div>
          <div className="tutor-main-2">
            <Feed
              component={TestQuote}
              data_type={data_type}
              offset={0}
              paging={paging}
            />
            {
              initArea(
                "tutor-tests",
                { ...this.props },
              )
            }
          </div>
          <div className="tutor-right-aside-2">
            {initArea("tutor-tests-right-aside",
              { ...this.props })}
          </div>
        </div>
      </div>
    )
  }
}
export default compose(
  withApollo,
  withRouter,
)(TutorTests)
