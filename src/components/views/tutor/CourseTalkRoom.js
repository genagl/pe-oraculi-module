import { Callout } from "@blueprintjs/core"
import React, { Component, } from "react"
import { withRouter } from "react-router"
import { compose } from "recompose"
import { moduleExists } from "react-pe-layouts"
import { __ } from "react-pe-utilities" 
// if (pluginExists("pe-jitsi-module")) {
// 	import Translation from "../../pe-jitsi-module/views/translationState/Translation"
// }


class CourseTalkRoom extends Component {
	render() {
		const { jitsi, jitsi_password, post_title, user } = this.props
		const translation = moduleExists("pe-jitsi-module")
			?
			// <Translation
			// 	user={user}
			// 	translation={{
			// 		post_title,
			// 		current: jitsi,
			// 		external_id: jitsi,
			// 		password: jitsi_password,
			// 		is_locked: true,
			// 		pe_room: [{
			// 			external_id: jitsi,
			// 			members: [],
			// 		}],
			// 	}}
			// 	onLeave={this.onLeave}
			// 	onChange={this.onChange}
			// 	onJoin={this.onJoin}
			// 	participantJoined={this.participantJoined}
			// 	participantKickedOut={this.participantKickedOut}
			// 	participantLeft={this.participantLeft}
			// 	feedbackSubmitted={this.feedbackSubmitted}
			// />
			null
			:
			<Callout>
				{__("Need plugin PE Jitsi")}
			</Callout>
		return (
			<div className="course-talk-room-cont">
				{translation}
			</div>
		)
	}

	onLeave = (evt) => {
		if (this.props.onLeave) this.props.onLeave(evt)
	}

	onChange = (name, data) => {
		if (this.props.onChange) this.props.onChange(name, data)
	}

	onJoin = (data) => {
		if (this.props.onJoin) this.props.onJoin(data)
	}

	participantJoined = (data) => {
		if (this.props.participantJoined) this.props.participantJoined(data)
	}

	participantKickedOut = (data) => {
		if (this.props.participantKickedOut) this.props.participantKickedOut(data)
	}

	participantLeft = (data) => {
		if (this.props.participantLeft) this.props.participantLeft(data)
	}

	feedbackSubmitted = (data) => {
		if (this.props.feedbackSubmitted) this.props.feedbackSubmitted(data)
	}
}

export default compose(
	withRouter,
)(CourseTalkRoom)
