import React, { Component } from "react"
import { NavLink } from "react-router-dom"
import { __ } from "react-pe-utilities" 
import { initArea } from "react-pe-utilities" 

class FacultetCard extends Component {
  render() {
    const quote = `${this.props.post_content
      .split(" ")
      .slice(0, 14)
      .join(" ")
						 }...`
    return (
      <div className="course-card">
        <div className="course-thumb" style={{ backgroundImage: `url(${this.props.thumbnail})` }} />
        {
				initArea("facultet-card-header", { ...this.props })
			}
        <div className="course-title">
          { this.props.post_title }
        </div>
        {
				initArea("facultet-card-after-title", { ...this.props })
			}
        <div className="course-quote">
          { quote }
        </div>
        {
				initArea("facultet-card-footer", { ...this.props })
			}
        <NavLink
          to={`/facultet/${this.props.id}`}
          className="course-link"
        >
          {__("More")}
        </NavLink>
      </div>
    )
  }
}

export default FacultetCard
