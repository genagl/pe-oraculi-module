import React from "react" 
import { Query } from "react-apollo" 
import BasicState from "react-pe-basic-view"
import { __ } from "react-pe-utilities" 
import {Loading} from 'react-pe-useful'
import { getQueryArgs, getQueryName, queryCollection } from "react-pe-layouts"
import ThemeCard from "./tutor/ThemeCard"

class TutorThemes extends BasicState {
	getRoute = () => "themes"

	addRender() {
	  const query_name = getQueryName("Bio_Biology_Theme")
	  const query_args = getQueryArgs("Bio_Biology_Theme")
	  const query = queryCollection("Bio_Biology_Theme", query_name, query_args)

	  return (
  <div className="course-card-list">
    <Query query={query}>
      {
				({
				  loading, error, data, client,
				}) => {
				  if (loading) {
				    return <Loading />
				  }
				  if (data) {
				    const current_course_id = this.props.user
				      ? this.props.user.current_course ? this.props.user.current_course.id : -1
				      : -1
				    const current_course_title = this.props.user
				      ? this.props.user.current_course ? this.props.user.current_course.post_title : ""
				      : ""

				    const courses = data[query_name].filter((e) => e.parent == 0 || !e.parent)
				    console.log(courses)
				    const coursesList = courses.map((e, i) => <ThemeCard user={this.props.user} {...e} key={i} i={i} />)
				    return (
  <div className="themes-card-list">
    {coursesList}
  </div>
				    )
				  }
				}
			}
    </Query>
  </div>
	  )
	}
}
export default (TutorThemes)
