function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { Query, withApollo } from "react-apollo";
import gql from "graphql-tag";
import { Loading } from 'react-pe-useful';
import BasicState from "react-pe-basic-view"; // import { getQueryArgs, getQueryName } from "react-pe-layouts"

import { initArea } from "react-pe-utilities";
import Advantages from "./tutor/Advantages";
import CourseCard from "./tutor/CourseCard";

class TutorCourse extends BasicState {
  constructor(props) {
    super(props);

    _defineProperty(this, "prev", () => {
      console.log(this.car.current.prev);
      this.car.current.prev();
    });

    _defineProperty(this, "next", () => {
      this.car.current.next();
    });

    this.car = /*#__PURE__*/React.createRef();
  }

  basic_state_data() {
    return {
      height: 500
    };
  } // TODO курсы


  render() {
    //const query_name = getQueryName("Bio_Course")
    //const query_args = getQueryArgs("Bio_Course")
    // const query = querySingle( "Bio_course", query_name, query_args );
    const {
      id
    } = this.props.match.params;
    const query = gql`query getBio_Facultet($id:String) {
			getBio_Facultet(id:$id) {
				id
				post_title
				post_content
				thumbnail
				icon
				post_date
				parent
				{
					id
					post_title
				}
				price
				order
				children {
					id
					post_title
					post_content
					__typename
				}
				bio_advantage
				{
					id
					post_title
					post_content
					thumbnail
				}
				bio_image_result
				{
					id
					post_title
					post_content
					thumbnail
				}
				courses
				{
					id
					post_title
					post_content
					is_member
					is_closed
					thumbnail
				}
				__typename
			}
		  }
		  `;
    return /*#__PURE__*/React.createElement("div", {
      className: "layout-state p-0"
    }, /*#__PURE__*/React.createElement("div", {
      className: "position-relative "
    }, /*#__PURE__*/React.createElement(Query, {
      query: query,
      variables: {
        id
      }
    }, ({
      loading,
      error,
      data,
      client
    }) => {
      if (loading) {
        return /*#__PURE__*/React.createElement("div", {
          className: "layout-state bg-white"
        }, /*#__PURE__*/React.createElement(Loading, null));
      }

      if (data) {
        console.log(data.getBio_Facultet);
        const facultet = data.getBio_Facultet ? data.getBio_Facultet : {};
        const pic = facultet.icon;
        const thum = facultet.thumbnail;
        const coursesList = facultet.courses.map((e, i) => /*#__PURE__*/React.createElement(CourseCard, _extends({
          user: this.props.user
        }, e, {
          key: i,
          i: i
        })));
        return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
          className: "course__header",
          style: {
            backgroundImage: `url(${thum})`
          }
        }, /*#__PURE__*/React.createElement("div", {
          className: "z-index-10 container"
        }, /*#__PURE__*/React.createElement("div", {
          className: "tutor-large-icon",
          style: {
            backgroundImage: `url(${pic})`
          }
        }), /*#__PURE__*/React.createElement("div", {
          className: "title text-light"
        }, facultet.post_title))), /*#__PURE__*/React.createElement("div", {
          className: " mt-5"
        }, /*#__PURE__*/React.createElement("div", {
          className: "tutor-row"
        }, /*#__PURE__*/React.createElement("div", {
          className: "tutor-left-aside-2  mobile-relatived"
        }, initArea("single-facultet-left-aside", { ...facultet,
          user: this.props.user
        })), /*#__PURE__*/React.createElement("div", {
          className: "tutor-main-2"
        }, /*#__PURE__*/React.createElement("div", {
          className: "row mb-5"
        }, /*#__PURE__*/React.createElement(Advantages, {
          advantages: facultet.bio_advantage,
          columns: 3
        })), /*#__PURE__*/React.createElement("div", {
          className: "r-course-underline",
          dangerouslySetInnerHTML: {
            __html: facultet.post_content
          }
        }), /*#__PURE__*/React.createElement("div", {
          className: "my-5 course-card-list count-2"
        }, coursesList), initArea("single_facultet", { ...facultet,
          user: this.props.user
        })), /*#__PURE__*/React.createElement("div", {
          className: "tutor-right-aside-2"
        }, initArea("single-facultet-right-aside", { ...facultet,
          user: this.props.user
        })))));
      }

      if (error) {
        return error.toString();
      }
    })));
  }

}

export default compose(withApollo, withRouter)(TutorCourse);