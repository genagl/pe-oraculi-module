function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { Query, withApollo } from "react-apollo";
import gql from "graphql-tag";
import TestResultGroup from "./tutor/TestResultGroup";
import { Loading } from 'react-pe-useful';
import BasicState from "react-pe-basic-view";
import { __ } from "react-pe-utilities";
import { initArea } from "react-pe-utilities";

class TutorUserTestResults extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "getRoute", () => "tests-tesult");

    _defineProperty(this, "onOpen", id => {
      this.setState({
        id
      });
    });
  }

  basic_state_data() {
    return {
      id: -1
    };
  }

  addRender() {
    if (!this.props.user) {
      return /*#__PURE__*/React.createElement("div", {
        className: "alert alert-danger p-5"
      }, __("You must logg in!"));
    }

    const {
      id
    } = this.props.user;
    const query = gql`query getBio_UserTest($id:String)  
		{
			getBio_UserTest(id:$id) 
			{				
				id
				test
				{
				  id 
				  post_title
				}
				credits
				right_count
				start_time
				end_time
				bio_test_category
				{
				  id
				  post_title
				}
			}
		}`;
    return /*#__PURE__*/React.createElement("div", {
      className: "layout-state p-0"
    }, /*#__PURE__*/React.createElement(Query, {
      query: query,
      variables: {
        id
      }
    }, ({
      loading,
      error,
      data,
      client
    }) => {
      if (loading) {
        return /*#__PURE__*/React.createElement(Loading, null);
      }

      if (data) {
        console.log(data.getBio_UserTest);
        const results = data.getBio_UserTest || [];
        const testGroup = [];
        const ress = results.forEach((e, i) => {
          if (!testGroup[e.test.id]) testGroup[e.test.id] = [];
          testGroup[e.test.id].push(e);
        });
        console.log(testGroup);
        const groups = testGroup.map((e, i) =>
        /*#__PURE__*/
        // console.log(e);
        React.createElement(TestResultGroup, {
          tests: e,
          key: i,
          openId: this.state.id,
          onOpen: this.onOpen
        }));
        return /*#__PURE__*/React.createElement("div", {
          className: " mt-5"
        }, /*#__PURE__*/React.createElement("div", {
          className: "tutor-row"
        }, /*#__PURE__*/React.createElement("div", {
          className: "tutor-left-aside-3 mobile-relatived"
        }, initArea("user-test-results-left-aside", { ...this.props,
          results
        })), /*#__PURE__*/React.createElement("div", {
          className: "tutor-main-3"
        }, /*#__PURE__*/React.createElement("div", {
          className: "row test-result bg-secondary text-light"
        }, /*#__PURE__*/React.createElement("div", {
          className: "col-md-6"
        }, __("Test")), /*#__PURE__*/React.createElement("div", {
          className: "col-md-1"
        }, __("Times")), /*#__PURE__*/React.createElement("div", {
          className: "col-md-1"
        }, __("Credits")), /*#__PURE__*/React.createElement("div", {
          className: "col-md-4"
        }, __("duration"))), groups, initArea("user-test-results", { ...this.props,
          results
        })), /*#__PURE__*/React.createElement("div", {
          className: "tutor-right-aside-3"
        }, initArea("user-test-results-right-aside", { ...this.props,
          results
        }))));
      }

      if (error) {
        return error.toString();
      }
    }));
  }

}

export default compose(withApollo, withRouter)(TutorUserTestResults);