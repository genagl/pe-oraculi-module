function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Suspense } from "react";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { Query, withApollo } from "react-apollo";
import { NavLink } from "react-router-dom";
import gql from "graphql-tag";
import RaitingLabel from "./tutor/RaitingLabel";
import { initArea } from "react-pe-utilities";
import { __ } from "react-pe-utilities";
import BasicState from "react-pe-basic-view";
import { getQueryArgs, getQueryName } from "react-pe-layouts";
import { Loading } from 'react-pe-useful'; //const OwlCarousel = React.lazy(() => import( 'react-owl-carousel2fix' ))

import { Carousel as ElCarousel } from '@trendyol-js/react-carousel';

class TutorCourse extends BasicState {
  constructor(props) {
    super(props);

    _defineProperty(this, "prev", () => {
      console.log(this.car.current.prev);
      this.car.current.prev();
    });

    _defineProperty(this, "next", () => {
      this.car.current.next();
    });
  }

  basic_state_data() {
    return {
      height: 500
    };
  } // TODO курсы


  render() {
    // const query_name = getQueryName("Bio_Course")
    // const query_args = getQueryArgs("Bio_Course")
    // const query = querySingle( "Bio_course", query_name, query_args );
    const {
      id
    } = this.props.match.params;
    const query = gql`query getBio_Course($id:String) {
			getBio_Course(id:$id) {
				id
				post_title
				post_content
				date
				is_member
				is_closed
				thumbnail
				logotype
				post_date
				parent
				{
					id
					post_title
				}
				bio_facultet
				{
					id
					post_title
				}
				price
				raiting
				children {
					id
					post_title
					post_content
					children 
					{
						id
						post_title
						post_content
						children 
						{
							id
							post_title
							post_content
							children 
							{
								id
								post_title
								post_content
								children 
								{
									id
									post_title
									post_content
									children 
									{
										id
										post_title
										post_content
									}
								}
							}
						}
					}
					__typename
				}
				articles
				{
					id
					post_title
					post_content
					is_free
					is_logged_in
					thumbnail
					is_favorite
				}
				testinomials {
					id
					post_title
					post_content
					thumbnail
					raiting				
					display_name
					__typename
				}
				__typename
			}
		  }
		  `;
    return /*#__PURE__*/React.createElement("div", {
      className: "layout-state p-0"
    }, /*#__PURE__*/React.createElement("div", {
      className: "position-relative "
    }, /*#__PURE__*/React.createElement(Query, {
      query: query,
      variables: {
        id
      }
    }, ({
      loading,
      error,
      data,
      client
    }) => {
      if (loading) {
        return /*#__PURE__*/React.createElement(Loading, null);
      }

      if (data) {
        console.log(data.getBio_Course);
        const course = data.getBio_Course ? data.getBio_Course : {};
        const options = {
          loop: true,
          margin: 30,
          items: 2,
          responsive: {
            0: {
              items: 1
            },
            1000: {
              items: 1
            },
            1200: {
              items: 2
            }
          }
        };
        const coursesList = course.testinomials.map((ee, ii) => /*#__PURE__*/React.createElement("div", {
          className: "review-slider__item",
          key: ii
        }, /*#__PURE__*/React.createElement("div", {
          className: "review-slider__item-head"
        }, /*#__PURE__*/React.createElement("div", {
          className: "review-slider__item-img",
          style: {
            backgroundImage: `url(${ee.thumbnail})`
          }
        }), /*#__PURE__*/React.createElement("div", {
          className: "review-slider__item-box"
        }, /*#__PURE__*/React.createElement("div", {
          className: "review-slider__item-name"
        }, ee.display_name), /*#__PURE__*/React.createElement("div", {
          className: "position-relative "
        }, /*#__PURE__*/React.createElement(RaitingLabel, {
          raiting: ee.raiting,
          className: "text-warning "
        })))), /*#__PURE__*/React.createElement("div", {
          className: "review-slider__item-content ",
          dangerouslySetInnerHTML: {
            __html: ee.post_content
          }
        })));
        const testinomials = coursesList.length > 0 ? /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
          className: "col-12 mt-3"
        }, /*#__PURE__*/React.createElement("div", {
          className: "title"
        }, __("Отзывы о курсе"))), /*#__PURE__*/React.createElement("div", {
          className: "col-12 slick-slider"
        }, /*#__PURE__*/React.createElement("div", {
          className: "slider-arrows"
        }, /*#__PURE__*/React.createElement("div", {
          className: "slick-arrow to-left",
          onClick: this.next
        }, /*#__PURE__*/React.createElement("i", {
          className: "fas fa-arrow-right"
        })), /*#__PURE__*/React.createElement("div", {
          className: "slick-arrow to-right",
          onClick: this.prev
        }, /*#__PURE__*/React.createElement("i", {
          className: "fas fa-arrow-left"
        }))), /*#__PURE__*/React.createElement("div", {
          className: "slider-cont"
        }, /*#__PURE__*/React.createElement("div", {
          className: "row w-100 mb-5"
        }, /*#__PURE__*/React.createElement(Suspense, {
          fallback: /*#__PURE__*/React.createElement(Loading, null)
        }, /*#__PURE__*/React.createElement(ElCarousel, {
          show: options.items,
          swiping: true,
          swipeOn: -50,
          responsive: true,
          className: "landing-carousel-container",
          useArrowKeys: false,
          infinite: options.loop
        }, coursesList)))))) : null;
        const pic = course.thumbnail !== "false" ? course.thumbnail : "";
        console.log(course.post_title);
        return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
          className: "course__header",
          style: {
            backgroundImage: `url(${pic})`
          }
        }, /*#__PURE__*/React.createElement("div", {
          className: "z-index-10 container"
        }, /*#__PURE__*/React.createElement("div", {
          className: "title text-light"
        }, course.post_title, course.bio_facultet && course.bio_facultet.id ? /*#__PURE__*/React.createElement("div", {
          className: "small"
        }, /*#__PURE__*/React.createElement(NavLink, {
          className: "text-light",
          to: `/facultet/${course.bio_facultet.id}`
        }, /*#__PURE__*/React.createElement("span", {
          className: "thin"
        }, `${__("Facultet")}: `), course.bio_facultet.post_title)) : null))), /*#__PURE__*/React.createElement("div", {
          className: " mt-5"
        }, /*#__PURE__*/React.createElement("div", {
          className: "tutor-row"
        }, /*#__PURE__*/React.createElement("div", {
          className: "tutor-left-aside  mobile-relatived"
        }, initArea("single-course-left-aside", { ...course,
          user: this.props.user,
          type: "Bio_Course"
        })), /*#__PURE__*/React.createElement("div", {
          className: "tutor-main"
        }, /*#__PURE__*/React.createElement(RaitingLabel, {
          raiting: course.raiting,
          className: "text-warning font_18 mt-5"
        }), /*#__PURE__*/React.createElement("div", {
          className: "r-course-underline",
          dangerouslySetInnerHTML: {
            __html: course.post_content
          }
        }), initArea("single_course", { ...course,
          user: this.props.user,
          type: "Bio_Course"
        }), testinomials), /*#__PURE__*/React.createElement("div", {
          className: "tutor-right-aside"
        }, initArea("single-course-right-aside", { ...course,
          user: this.props.user,
          type: "Bio_Course"
        })))));
      }

      if (error) {
        return error.toString();
      }
    })));
  }

}

export default compose(withApollo, withRouter)(TutorCourse);