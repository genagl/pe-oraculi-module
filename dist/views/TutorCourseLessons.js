function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { Query, withApollo } from "react-apollo";
import { NavLink } from "react-router-dom";
import gql from "graphql-tag";
import CourseArticleList from "./tutor/CourseArticleList";
import CourseTestList from "./tutor/CourseTestList";
import CourseEventList from "./tutor/CourseEventList";
import CourseTalkRoom from "./tutor/CourseTalkRoom";
import CourseLessons from "./tutor/CourseLessons";
import { Loading } from 'react-pe-useful';
import { __ } from "react-pe-utilities";
import BasicState from "react-pe-basic-view";

class TutorCourseLessons extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "getRoute", () => "course-lessons");

    _defineProperty(this, "onTab", evt => {
      const tab = evt.currentTarget.getAttribute("tab");
      this.setState({
        tab
      });
    });

    _defineProperty(this, "onLeave", () => {
      this.setState({
        tab: this.state.tabs[0].name
      });
    });
  }

  basic_state_data() {
    return {
      tabs: [{
        name: "articles",
        title: "Articles",
        component: CourseArticleList
      }, {
        name: "tests",
        title: "Tests",
        component: CourseTestList
      }, {
        name: "events",
        title: "Events",
        component: CourseEventList
      }, {
        name: "talkroom",
        title: "Talk Room",
        component: CourseTalkRoom
      }]
    };
  }

  render() {
    const {
      id
    } = this.props.match.params;
    const query = gql`query getBio_Course($id:String)
		{
			getBio_Course(id:$id)
			{
				id
				post_title
				post_content
				date
				thumbnail
				logotype
				jitsi
				jitsi_password
				post_date
				includes
				is_member
				is_closed
				bio_facultet
				{
					id
					post_title
				}
				articles
				{
					id
					post_title
					post_content
					is_free
					is_logged_in
					thumbnail
					is_favorite
				}
				parent
				{
					id
					post_title
				}
				children {
					id
					post_title
					post_content
					children 
					{
						id
						post_title
						post_content
						children 
						{
							id
							post_title
							post_content
							children 
							{
								id
								post_title
								post_content
								children 
								{
									id
									post_title
									post_content
									children 
									{
										id
										post_title
										post_content
									}
								}
							}
						}
					}
					__typename
				}
				bio_facultet
				{
					id
					post_title
				}
				
				__typename
			}
		}`;
    return /*#__PURE__*/React.createElement("div", {
      className: "layout-state p-0"
    }, /*#__PURE__*/React.createElement(Query, {
      query: query,
      variables: {
        id
      }
    }, ({
      loading,
      error,
      data,
      client
    }) => {
      if (loading) {
        return /*#__PURE__*/React.createElement(Loading, null);
      }

      if (data) {
        console.log(data.getBio_Course);
        const course = data.getBio_Course || {};
        const pic = course.thumbnail != "false" ? course.thumbnail : "";
        return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
          className: "course__header",
          style: {
            backgroundImage: `url(${pic})`
          }
        }, /*#__PURE__*/React.createElement("div", {
          className: "z-index-10 container"
        }, /*#__PURE__*/React.createElement("div", {
          className: "title text-light"
        }, course.post_title, course.bio_facultet && course.bio_facultet.id ? /*#__PURE__*/React.createElement("div", {
          className: "small"
        }, /*#__PURE__*/React.createElement(NavLink, {
          className: "text-light",
          to: `/facultet/${course.bio_facultet.id}`
        }, /*#__PURE__*/React.createElement("span", {
          className: "thin"
        }, `${__("Facultet")}: `), course.bio_facultet.post_title)) : null))), /*#__PURE__*/React.createElement(CourseLessons, {
          tabs: this.state.tabs,
          route: this.getRoute(),
          user: this.props.user,
          course: course,
          id: this.props.match.params.id,
          onLeave: this.onLeave
        }));
      }

      if (error) {
        return error.toString();
      }
    }));
  }

}

export default compose(withApollo, withRouter)(TutorCourseLessons);