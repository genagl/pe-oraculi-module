function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { Query, withApollo } from "react-apollo";
import Moment from "react-moment";
import gql from "graphql-tag";
import { __ } from "react-pe-utilities";
import BasicState from "react-pe-basic-view";
import { Loading } from 'react-pe-useful';
import { initArea } from "react-pe-utilities";

class TutorLesson extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "getRoute", () => "course-lessons");
  }

  render() {
    const {
      id
    } = this.props.match.params;
    const query = gql`query getBio_Article($id:String) {
			getBio_Article(id:$id) { 
				id
				post_title
				post_content
				post_date
				thumbnail
				video
				include_id1
				include_id2
				include_id3
				include_id4
				post_author
				{
				  id
				  display_name
				  avatar
				}
				bio_course
				{
				  id
				  post_title
				  logotype
				  children
				  {
					  id
					  post_title					  
					  children
					  {
						  id
						  post_title
						  
					  }
				  }
				}
				bio_olimpiad_type
				{
				  id
				  post_title
				}
				bio_class
				{
				  id
				  post_title
				}
				bio_biology_theme
				{
				  id
				  post_title
				  thumbnail
				}
				comments
				{
					id
					discussion_id
					discussion_type
					content
					parent_id
					author
					{
						avatar
						display_name
						id
					}
					date
					is_approved
					
				}
			  }
			  __typename
		  }
		  `;
    return /*#__PURE__*/React.createElement("div", {
      className: "layout-state"
    }, /*#__PURE__*/React.createElement(Query, {
      query: query,
      variables: {
        id
      }
    }, ({
      loading,
      error,
      data,
      client
    }) => {
      if (loading) {
        return /*#__PURE__*/React.createElement("div", {
          className: "layout-state bg-white"
        }, /*#__PURE__*/React.createElement(Loading, null));
      }

      if (data) {
        console.log(data.getBio_Article);
        const article = data.getBio_Article || {};
        const classes = article.bio_class.map((e, i) => /*#__PURE__*/React.createElement("div", {
          className: "tutor-label",
          key: i
        }, `${e.post_title} ${__("class")}`));
        const themes = article.bio_biology_theme.map((e, i) => /*#__PURE__*/React.createElement("div", {
          className: "tutor-label",
          key: i
        }, e.post_title));
        const courses = article.bio_course.map((e, i) => /*#__PURE__*/React.createElement("div", {
          className: "tutor-label",
          key: i
        }, e.post_title));
        const thumbnail = article.thumbnail ? /*#__PURE__*/React.createElement("div", {
          className: "arcicle-thumbnail",
          style: {
            backgroundImage: `url(${article.thumbnail})`
          }
        }) : null;
        return /*#__PURE__*/React.createElement("div", {
          className: " mt-5",
          "article-id": article.id
        }, /*#__PURE__*/React.createElement("div", {
          className: "tutor-row"
        }, /*#__PURE__*/React.createElement("div", {
          className: "tutor-left-aside  mobile-relatived"
        }, initArea("single-article-left-aside", { ...article,
          user: this.props.user,
          prefix: "articles"
        })), /*#__PURE__*/React.createElement("div", {
          className: "tutor-main"
        }, /*#__PURE__*/React.createElement("div", {
          className: "tutor-article-title"
        }, article.post_title), /*#__PURE__*/React.createElement("div", {
          className: "lesson-classes"
        }, classes), /*#__PURE__*/React.createElement("div", {
          className: "lesson-themes"
        }, themes), /*#__PURE__*/React.createElement("div", {
          className: "lesson-courses"
        }, courses), /*#__PURE__*/React.createElement("div", {
          className: "",
          dangerouslySetInnerHTML: {
            __html: article.post_content
          }
        }), /*#__PURE__*/React.createElement("div", {
          className: "lesson-author"
        }, /*#__PURE__*/React.createElement("div", {
          className: "avatar-large",
          style: {
            backgroundImage: `url(${article.post_author.avatar})`
          }
        }), /*#__PURE__*/React.createElement("div", {
          className: ""
        }, /*#__PURE__*/React.createElement("div", {
          className: "display-name"
        }, article.post_author.display_name), /*#__PURE__*/React.createElement("div", {
          className: ""
        }, /*#__PURE__*/React.createElement(Moment, {
          locale: "ru",
          format: "D MMMM YYYY"
        }, new Date(article.post_date * 1000))))), /*#__PURE__*/React.createElement("div", {
          className: "px-3 pt-3 borded-top "
        }), initArea("single-article", { ...article,
          user: this.props.user,
          prefix: "articles"
        })), /*#__PURE__*/React.createElement("div", {
          className: "tutor-right-aside"
        }, thumbnail, initArea("single-article-right-aside", { ...article,
          user: this.props.user,
          prefix: "articles"
        }))));
      }

      if (error) {
        return error.toString();
      }
    }));
  }

}

export default compose(withApollo, withRouter)(TutorLesson);