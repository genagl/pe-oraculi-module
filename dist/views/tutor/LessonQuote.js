import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { __ } from "react-pe-utilities";
import { initArea } from "react-pe-utilities";

class LessonQuote extends Component {
  render() {
    const cotent = `${this.props.post_content.split(" ").slice(0, 14).join(" ").replace(/(<\S([^>]+)>)/ig, "")}...`;
    const thumb = /*#__PURE__*/React.createElement("div", {
      className: "lesson-quote-thumb",
      style: {
        backgroundImage: `url(${this.props.thumbnail})`
      }
    });
    const classes = this.props.bio_class.map((e, i) => /*#__PURE__*/React.createElement("div", {
      className: "tutor-label",
      key: i
    }, `${e.post_title} ${__("class")}`));
    const themes = this.props.bio_biology_theme.map((e, i) => /*#__PURE__*/React.createElement("div", {
      className: "tutor-label",
      key: i
    }, e.post_title));
    const courses = this.props.bio_course.map((e, i) => /*#__PURE__*/React.createElement("div", {
      className: "tutor-label",
      key: i
    }, e.post_title));
    return /*#__PURE__*/React.createElement("div", {
      className: "lesson-quote-cont"
    }, initArea("lesson-quote-header", { ...this.props
    }), /*#__PURE__*/React.createElement(NavLink, {
      className: "lesson-quote-first",
      to: `/article/${this.props.id}`
    }, thumb), /*#__PURE__*/React.createElement("div", {
      className: "lesson-quote-second"
    }, initArea("lesson-quote-before-title", { ...this.props
    }), /*#__PURE__*/React.createElement(NavLink, {
      className: "lesson-qoute-title",
      to: `/article/${this.props.id}`
    }, __(this.props.post_title)), /*#__PURE__*/React.createElement("div", {
      className: "lesson-quote-content",
      dangerouslySetInnerHTML: {
        __html: cotent
      }
    }), /*#__PURE__*/React.createElement("div", {
      className: ""
    }, /*#__PURE__*/React.createElement(NavLink, {
      className: "",
      to: `/article/${this.props.id}`
    }, __("More"))), /*#__PURE__*/React.createElement("div", {
      className: "d-flex flex-wrap"
    }, /*#__PURE__*/React.createElement("div", {
      className: "lesson-qoute-classes"
    }, classes), /*#__PURE__*/React.createElement("div", {
      className: "lesson-qoute-themes"
    }, themes), /*#__PURE__*/React.createElement("div", {
      className: "lesson-qoute-courses"
    }, courses)), /*#__PURE__*/React.createElement("div", {
      className: "lesson-quote-footer"
    }, initArea("lesson-quote-footer", { ...this.props
    }))));
  }

}

export default LessonQuote;