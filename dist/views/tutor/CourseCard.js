import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { __ } from "react-pe-utilities";

class CourseCard extends Component {
  render() {
    const quote = `${this.props.post_content.split(" ").slice(0, 14).join(" ")}...`;
    const nav = this.props.is_member ? /*#__PURE__*/React.createElement(NavLink, {
      to: `/course-lessons/${this.props.id}`,
      className: "course-lessond-link"
    }, __("Continue learning")) : /*#__PURE__*/React.createElement(NavLink, {
      to: `/course/${this.props.id}`,
      className: "course-link"
    }, __("More"));
    const title = this.props.is_member ? /*#__PURE__*/React.createElement(NavLink, {
      className: "course-title",
      to: `/course-lessons/${this.props.id}`
    }, /*#__PURE__*/React.createElement("div", {
      className: "course-thumb",
      style: {
        backgroundImage: `url(${this.props.thumbnail})`
      }
    }), this.props.post_title) : /*#__PURE__*/React.createElement(NavLink, {
      className: "course-title",
      to: `/course/${this.props.id}`
    }, /*#__PURE__*/React.createElement("div", {
      className: "course-thumb",
      style: {
        backgroundImage: `url(${this.props.thumbnail})`
      }
    }), this.props.post_title);
    return /*#__PURE__*/React.createElement("div", {
      className: "course-card"
    }, title, /*#__PURE__*/React.createElement("div", {
      className: "course-quote"
    }, quote), nav);
  }

}

export default CourseCard;