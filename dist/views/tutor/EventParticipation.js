function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Button, ButtonGroup, Dialog, Intent } from "@blueprintjs/core";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import gql from "graphql-tag";
import { __ } from "react-pe-utilities";

class EventParticipation extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onToggle", () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    });

    _defineProperty(this, "onWithdraw", () => {
      //
      const Withdraw_Request = gql`mutation Withdraw_Request ( $input: EventRequestInput ) 
		{
		  Withdraw_Request(input : $input)
		  
		}`;
      const pr = {};
      pr.user = this.props.user.id;
      pr.event = this.props.id;
      this.props.client.mutate({
        mutation: Withdraw_Request,
        variables: {
          input: pr
        },
        update: (store, {
          data: {
            Withdraw_Request
          }
        }) => {
          console.log(Withdraw_Request);
          this.setState({
            isOpen: false,
            member_status: Withdraw_Request ? 0 : this.state.member_status
          });
        }
      });
    });

    _defineProperty(this, "onApply", () => {
      const Apply_Request = gql`mutation Apply_Request ( $input: EventRequestInput ) 
		{
		  Apply_Request(input : $input)
		  
		}`;
      const pr = {};
      pr.user = this.props.user.id;
      pr.event = this.props.id;
      this.props.client.mutate({
        mutation: Apply_Request,
        variables: {
          input: pr
        },
        update: (store, {
          data: {
            Apply_Request
          }
        }) => {
          console.log(Apply_Request);
          this.setState({
            isOpen: false,
            member_status: Apply_Request ? 1 : this.state.member_status
          });
        }
      });
    });

    this.state = {
      isOpen: false,
      member_status: this.props.member_status
    };
  }

  render() {
    console.log(this.props.time * 1000, Date.now());

    if (this.props.time * 1000 < Date.now()) {
      return /*#__PURE__*/React.createElement("div", {
        className: "alert alert-warning request-event"
      }, /*#__PURE__*/React.createElement("div", null, __("Event archived")));
    }

    switch (this.state.member_status) {
      case 2:
        return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
          className: "alert alert-warning request-event"
        }, /*#__PURE__*/React.createElement("div", null, __("you are invited")), /*#__PURE__*/React.createElement("div", {
          className: "btn btn-warning btn-sm mt-5",
          onClick: this.onToggle
        }, __("Withdraw request"))), this.getDialog());

      case 1:
        return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
          className: "alert alert-warning request-event"
        }, /*#__PURE__*/React.createElement("div", null, __("your request is being processed")), /*#__PURE__*/React.createElement("div", {
          className: "btn btn-warning btn-sm mt-5",
          onClick: this.onToggle
        }, __("Withdraw request"))), this.getDialog());

      case 0:
      default:
        return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
          className: "btn btn-primary btn-lg add-event mt-4",
          onClick: this.onApply
        }, __("Apply for participation")));
    }
  }

  getDialog() {
    return /*#__PURE__*/React.createElement(Dialog, {
      title: __("Withdraw request"),
      isOpen: this.state.isOpen,
      onClose: this.onToggle
    }, /*#__PURE__*/React.createElement("div", {
      className: "p-4"
    }, /*#__PURE__*/React.createElement(ButtonGroup, null, /*#__PURE__*/React.createElement(Button, {
      intent: Intent.SUCCESS,
      onClick: this.onWithdraw
    }, __("Withdraw request")), /*#__PURE__*/React.createElement(Button, {
      intent: Intent.DANGER,
      onClick: this.onToggle
    }, __("Cancel")))));
  }

}

export default compose(withApollo)(EventParticipation);