import React, { Component } from "react";
import { __ } from "react-pe-utilities";

class Advantages extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let className = "4";

    switch (this.props.columns) {
      case 1:
        className = "12";
        break;

      case 2:
        className = "6";
        break;

      case 3:
        className = "4";
        break;

      case 4:
        className = "3";
        break;

      case 6:
        className = "2";
        break;

      case 12:
        className = "1";
        break;

      default:
        className = "4";
        break;
    }

    const _advantages = this.props.advantages.map((e, i) => /*#__PURE__*/React.createElement("div", {
      className: `col-lg-${className}`,
      key: i
    }, /*#__PURE__*/React.createElement("div", {
      className: "media"
    }, /*#__PURE__*/React.createElement("div", {
      className: "media-top m-2"
    }, /*#__PURE__*/React.createElement("img", {
      src: e.thumbnail,
      alt: "",
      height: "80px"
    })), /*#__PURE__*/React.createElement("div", {
      className: "media-bottom"
    }, /*#__PURE__*/React.createElement("div", {
      className: "media-title"
    }, __(e.post_title)), /*#__PURE__*/React.createElement("div", null, __(e.post_content))))));

    return /*#__PURE__*/React.createElement(React.Fragment, null, _advantages);
  }

}

export default Advantages;