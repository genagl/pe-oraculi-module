function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import $ from "jquery";
import { __ } from "react-pe-utilities";
import LessonComment from "./LessonComment";

class LessonMainComment extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      is_answer: false,
      answeHeight: 0,
      comment: ""
    });

    _defineProperty(this, "onAnswer", () => {
      this.setState({
        is_answer: !this.state.is_answer,
        answeHeight: !this.state.is_answer ? $(`#comment_${this.props._id} .answer textarea`).height() + 7 : 0
      });
    });

    _defineProperty(this, "onComment", evt => {
      const {
        value
      } = evt.currentTarget;
      this.setState({
        comment: value
      });
    });
  }

  render() {
    const {
      lesson,
      _id,
      title,
      content,
      author,
      date
    } = this.props;
    const first_comment = lesson.comments.filter(e => e.comment_id === _id)[0]; // const comments = lesson.comments
    // 	.filter(e => e.comment_id == _id)
    // 		.map((e, i) =>
    // 		{
    // 			return <LessonComment {...e} key={i} lesson={lesson} level={1} />
    // 		});

    const comments = /*#__PURE__*/React.createElement(LessonComment, _extends({
      key: 0
    }, first_comment, {
      lesson: lesson,
      level: 0
    }));
    const ava = author.avatar ? author.avatar : "/assets/img/def_ava.jpg";
    return /*#__PURE__*/React.createElement("div", {
      className: "comment",
      id: `comment_${_id}`
    }, /*#__PURE__*/React.createElement("div", {
      className: "d-flex"
    }, /*#__PURE__*/React.createElement("div", {
      className: "comment-ava",
      style: {
        backgroundImage: `url(${ava})`
      }
    }), /*#__PURE__*/React.createElement("div", {
      className: "title pl-0"
    }, title), /*#__PURE__*/React.createElement("div", {
      className: "date ml-auto"
    }, date)), /*#__PURE__*/React.createElement("div", {
      className: "content"
    }, content), /*#__PURE__*/React.createElement("div", {
      className: "answer d-flex",
      style: {
        height: this.state.answeHeight
      }
    }, /*#__PURE__*/React.createElement("textarea", {
      value: this.state.comment,
      onChange: this.onComment,
      rows: "4"
    }), /*#__PURE__*/React.createElement("div", {
      className: "ml-3 btn btn-primary"
    }, __("Ответить"))), /*#__PURE__*/React.createElement("div", {
      className: "d-flex author"
    }, /*#__PURE__*/React.createElement("span", {
      className: "showed",
      onClick: this.onAnswer
    }, __(this.state.is_answer ? "Закрыть" : "Ответить"))), comments, /*#__PURE__*/React.createElement("span", {
      className: "title-collapsed showed pointer btn-link"
    }, "\u0420\u0430\u0441\u043A\u0440\u044B\u0442\u044C"));
  }

}

export default LessonMainComment;