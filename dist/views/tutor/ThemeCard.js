import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { __ } from "react-pe-utilities";
import { initArea } from "react-pe-utilities";

class ThemeCard extends Component {
  render() {
    const quote = `${this.props.post_content.split(" ").slice(0, 14).join(" ")}...`;
    return /*#__PURE__*/React.createElement("div", {
      className: "theme-card"
    }, initArea("theme-card-header", { ...this.props
    }), /*#__PURE__*/React.createElement(NavLink, {
      to: `/theme-lessons/${this.props.id}`
    }, /*#__PURE__*/React.createElement("div", {
      className: "theme-icon",
      style: {
        backgroundImage: `url(${this.props.icon})`
      }
    }), /*#__PURE__*/React.createElement("div", {
      className: "theme-title"
    }, this.props.post_title), initArea("theme-card-after-title", { ...this.props
    })), /*#__PURE__*/React.createElement("div", {
      className: "theme-quote"
    }, quote), /*#__PURE__*/React.createElement("div", {
      className: ""
    }, /*#__PURE__*/React.createElement("div", {
      className: ""
    }, `${__("Articles")}: ${this.props.articles.length}`), /*#__PURE__*/React.createElement("div", {
      className: ""
    }, `${__("Tests")}: ${this.props.bio_test.length}`)), initArea("theme-card-footer", { ...this.props
    }), /*#__PURE__*/React.createElement(NavLink, {
      to: `/theme-lessons/${this.props.id}`,
      className: "theme-link"
    }, __("More")));
  }

}

export default ThemeCard;