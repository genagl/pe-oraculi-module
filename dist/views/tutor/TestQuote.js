import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Icon, Intent, Tooltip, Button, Position } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import { initArea } from "react-pe-utilities";

class TestQuote extends Component {
  render() {
    // const cotent = `${this.props.post_content
    //   .split(" ")
    //   .slice(0, 14)
    //   .join(" ")
    // 				 }...`
    // const thumb = <div className="lesson-quote-thumb" style={{ backgroundImage: `url(${this.props.thumbnail})` }} />
    const classes = this.props.bio_class.map((e, i) => /*#__PURE__*/React.createElement("div", {
      className: "tutor-label-class",
      key: i
    }, `${e.post_title} ${__("class")}`));
    const themes = this.props.bio_biology_theme.map((e, i) => /*#__PURE__*/React.createElement("div", {
      className: "tutor-label",
      key: i
    }, e.post_title));
    const courses = this.props.bio_course.map((e, i) => /*#__PURE__*/React.createElement("div", {
      className: "tutor-label-course",
      key: i
    }, e.post_title));
    const is_timed = this.props.is_timed ? /*#__PURE__*/React.createElement(Tooltip, {
      content: `${__("Time limit")}: ${this.props.duration} ${__("sec.")}`,
      className: "d-inline-block",
      position: Position.TOP_LEFT
    }, /*#__PURE__*/React.createElement(Button, {
      minimal: true,
      className: "m-1",
      title: `${__("Time limit")}: ${this.props.duration} ${__("sec.")}`,
      intent: Intent.SECONDARY
    }, /*#__PURE__*/React.createElement(Icon, {
      icon: "time"
    }))) : null;
    const is_show_answer = this.props.is_show_answer ? /*#__PURE__*/React.createElement(Tooltip, {
      content: __("Show right answer after choose"),
      className: "d-inline-block",
      position: Position.TOP_LEFT
    }, /*#__PURE__*/React.createElement(Button, {
      minimal: true,
      className: "m-1",
      title: __("Show right answer after choose"),
      intent: Intent.SECONDARY
    }, /*#__PURE__*/React.createElement(Icon, {
      icon: "eye-open"
    }))) : null;
    const is_show_results = this.props.is_show_results ? /*#__PURE__*/React.createElement(Tooltip, {
      content: __("Show result after test"),
      className: "d-inline-block",
      position: Position.TOP_LEFT
    }, /*#__PURE__*/React.createElement(Button, {
      minimal: true,
      className: "m-1",
      title: __("Show result after test"),
      intent: Intent.SECONDARY
    }, /*#__PURE__*/React.createElement(Icon, {
      icon: "issue"
    }))) : null;
    const questions_length = /*#__PURE__*/React.createElement(Tooltip, {
      content: __("Questions count"),
      className: "d-inline-block ",
      position: Position.TOP_LEFT
    }, /*#__PURE__*/React.createElement(Button, {
      minimal: true,
      className: "m-1 text-secondary font-weight-bold",
      title: __("Questions count"),
      intent: Intent.SECONDARY
    }, this.props.questions.length));
    return /*#__PURE__*/React.createElement("div", {
      className: "lesson-quote-cont"
    }, initArea("test-quote-header", { ...this.props
    }), /*#__PURE__*/React.createElement("div", {
      className: "lesson-quote-second"
    }, initArea("test-quote-before-title", { ...this.props
    }), /*#__PURE__*/React.createElement(NavLink, {
      className: "lesson-qoute-title",
      to: `/test/${this.props.id}`
    }, /*#__PURE__*/React.createElement("div", {
      dangerouslySetInnerHTML: {
        __html: this.props.post_title
      }
    })), /*#__PURE__*/React.createElement("div", {
      className: " pb-2 text-secondary"
    }, is_timed, is_show_answer, is_show_results, questions_length), /*#__PURE__*/React.createElement("div", {
      className: "d-flex flex-wrap"
    }, /*#__PURE__*/React.createElement("div", {
      className: "test-qoute-themes"
    }, themes), /*#__PURE__*/React.createElement("div", {
      className: "test-qoute-courses"
    }, courses), /*#__PURE__*/React.createElement("div", {
      className: "test-qoute-classes"
    }, classes), /*#__PURE__*/React.createElement("div", {
      className: "test-quote-footer"
    }, initArea("test-quote-footer", { ...this.props
    })))));
  }

}

export default TestQuote;