import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { __ } from "react-pe-utilities";
import { initArea } from "react-pe-utilities";

class FacultetCard extends Component {
  render() {
    const quote = `${this.props.post_content.split(" ").slice(0, 14).join(" ")}...`;
    return /*#__PURE__*/React.createElement("div", {
      className: "course-card"
    }, /*#__PURE__*/React.createElement("div", {
      className: "course-thumb",
      style: {
        backgroundImage: `url(${this.props.thumbnail})`
      }
    }), initArea("facultet-card-header", { ...this.props
    }), /*#__PURE__*/React.createElement("div", {
      className: "course-title"
    }, this.props.post_title), initArea("facultet-card-after-title", { ...this.props
    }), /*#__PURE__*/React.createElement("div", {
      className: "course-quote"
    }, quote), initArea("facultet-card-footer", { ...this.props
    }), /*#__PURE__*/React.createElement(NavLink, {
      to: `/facultet/${this.props.id}`,
      className: "course-link"
    }, __("More")));
  }

}

export default FacultetCard;