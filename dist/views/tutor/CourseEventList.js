import React, { Component } from "react";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import { __ } from "react-pe-utilities";
import EventQuote from "./EventQuote"; //import Feed from "../../../../layouts/BasicState/Feed" 

import { Feed } from "react-pe-basic-view";

class CourseTestList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      numberposts: 10,
      offset: 0
    };
  }

  render() {
    const {
      id,
      articles
    } = this.props;
    const data_type = "Bio_Event";
    const paging = `taxonomies :{ tax_name : "${this.props.type}", term_ids : [ ${id} ] }, order:"ASC"`;
    return /*#__PURE__*/React.createElement(Feed, {
      component: EventQuote,
      data_type: data_type,
      offset: 0,
      paging: paging
    });
  }

}

export default compose(withApollo, withRouter)(CourseTestList);