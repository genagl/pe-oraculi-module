function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { Component } from "react";
import { Route, Switch, withRouter } from "react-router";
import { compose } from "recompose";
import { NavLink } from "react-router-dom";
import { __ } from "react-pe-utilities";
import { initArea } from "react-pe-utilities";

class ClassLessons extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tab: "articles"
    };
  }

  render() {
    const {
      id,
      course,
      tabs,
      route,
      onLeave,
      user
    } = this.props;
    console.log(course);
    const url = `/${route}/${id}`;
    return /*#__PURE__*/React.createElement("div", {
      className: " mt-5"
    }, /*#__PURE__*/React.createElement("div", {
      className: "tutor-row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "tutor-left-aside  mobile-relatived"
    }, /*#__PURE__*/React.createElement("div", {
      className: "course_category_select mb-5"
    }, /*#__PURE__*/React.createElement("div", null, this.props.tabs.map((e, i) => /*#__PURE__*/React.createElement(NavLink, {
      className: "tutor-tab d-block ",
      activeClassName: " active ",
      exact: true,
      to: `${url}/${e.name}`,
      key: i
    }, __(e.title))))), initArea("single-course-lessons-left-aside", { ...course,
      user
    })), /*#__PURE__*/React.createElement("div", {
      className: "tutor-main"
    }, /*#__PURE__*/React.createElement(Switch, null, /*#__PURE__*/React.createElement(Route, {
      path: url,
      exact: true,
      component: routeProps => /*#__PURE__*/React.createElement("div", null, course.post_content)
    }), tabs.map((e, i) => {
      const ElTab = e.component;
      return /*#__PURE__*/React.createElement(Route, {
        path: `${url}/${e.name}`,
        key: i,
        exact: false,
        component: routeProps => /*#__PURE__*/React.createElement(ElTab, _extends({}, course, {
          user: user,
          onLeave: onLeave,
          type: this.props.type || "bio_class"
        }))
      });
    })), initArea("single-course-lessons", { ...course,
      user: this.props.user,
      type: "course"
    })), /*#__PURE__*/React.createElement("div", {
      className: "tutor-right-aside"
    }, initArea("single-course-lessons-right-aside", { ...course,
      user: this.props.user,
      type: "course"
    }))));
  }

}

export default compose(withRouter)(ClassLessons);