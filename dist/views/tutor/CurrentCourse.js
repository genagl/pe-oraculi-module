import React, { Component } from "react";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { __ } from "react-pe-utilities";
import { initArea } from "react-pe-utilities";

class CurrentCourse extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      course,
      user
    } = this.props;
    return /*#__PURE__*/React.createElement("div", {
      className: "container"
    }, /*#__PURE__*/React.createElement("div", null, initArea("curent-course-content", { ...this.props
    })));
  }

}

export default compose(withRouter)(CurrentCourse);