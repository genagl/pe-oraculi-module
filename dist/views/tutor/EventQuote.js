import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import Moment from "react-moment";
import { __ } from "react-pe-utilities";
import { initArea } from "react-pe-utilities";

class LessonQuote extends Component {
  render() {
    const cotent = `${this.props.post_content.split(" ").slice(0, 14).join(" ")}...`;
    const thumb = /*#__PURE__*/React.createElement("div", {
      className: "lesson-quote-thumb",
      style: {
        backgroundImage: `url(${this.props.thumbnail})`
      }
    });
    const themes = this.props.bio_biology_theme ? this.props.bio_biology_theme.map((e, i) => /*#__PURE__*/React.createElement("div", {
      className: "tutor-label",
      key: i
    }, e.post_title)) : null;
    const courses = this.props.bio_course ? this.props.bio_course.map((e, i) => /*#__PURE__*/React.createElement("div", {
      className: "tutor-label-course",
      key: i
    }, e.post_title)) : null;
    const classes = this.props.bio_class ? this.props.bio_class.map((e, i) => /*#__PURE__*/React.createElement("div", {
      className: "tutor-label-class",
      key: i
    }, `${e.post_title} ${__("class")}`)) : null;
    const bio_olimpiad_type = this.props.bio_olimpiad_type ? this.props.bio_olimpiad_type.map((e, i) => /*#__PURE__*/React.createElement("div", {
      className: "tutor-label-olympiad_type",
      key: i
    }, e.post_title)) : null;
    let invite;

    switch (this.props.member_status) {
      case 2:
        invite = /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
          className: "alert alert-warning request-event"
        }, /*#__PURE__*/React.createElement("div", null, __("you are invited"))));
        break;

      case 1:
        invite = /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
          className: "alert alert-warning request-event"
        }, /*#__PURE__*/React.createElement("div", null, __("your request is being processed"))));
        break;

      case 0:
      default:
        invite = /*#__PURE__*/React.createElement("div", null, "...");
    }

    const archived = this.props.time * 1000 < Date.now() ? /*#__PURE__*/React.createElement("div", {
      className: "diagonal1"
    }, __("Event archived")) : null;
    return /*#__PURE__*/React.createElement("div", {
      className: "lesson-quote-cont"
    }, initArea("event-quote-header", { ...this.props
    }), /*#__PURE__*/React.createElement(NavLink, {
      className: "lesson-quote-first ",
      to: `/event/${this.props.id}`
    }, thumb, archived), /*#__PURE__*/React.createElement("div", {
      className: "lesson-quote-second"
    }, initArea("event-quote-before-title", { ...this.props
    }), /*#__PURE__*/React.createElement(NavLink, {
      className: "lesson-qoute-title",
      to: `/event/${this.props.id}`
    }, __(this.props.post_title)), /*#__PURE__*/React.createElement("div", {
      className: ""
    }, /*#__PURE__*/React.createElement(Moment, {
      locale: "ru",
      format: "D MMMM YYYY"
    }, new Date(this.props.time * 1000))), /*#__PURE__*/React.createElement("div", {
      className: "lesson-quote-content",
      dangerouslySetInnerHTML: {
        __html: cotent
      }
    }), /*#__PURE__*/React.createElement("div", {
      className: ""
    }, /*#__PURE__*/React.createElement(NavLink, {
      className: "",
      to: `/event/${this.props.id}`
    }, __("More"))), /*#__PURE__*/React.createElement("div", {
      className: "lesson-qoute-themes"
    }, themes), /*#__PURE__*/React.createElement("div", {
      className: "lesson-qoute-classes"
    }, classes), /*#__PURE__*/React.createElement("div", {
      className: "lesson-qoute-courses"
    }, courses), /*#__PURE__*/React.createElement("div", {
      className: "lesson-qoute-olimpiad_type"
    }, bio_olimpiad_type), /*#__PURE__*/React.createElement("div", {
      className: "lesson-quote-footer"
    }, invite, initArea("event-quote-footer", { ...this.props
    }))));
  }

}

export default LessonQuote;