import { compose } from "recompose";
import { withRouter } from "react-router";
import BasicState from "react-pe-basic-view"; // import { __ } from "react-pe-utilities" 
//  if (moduleExists("pe-fest-module")) {
// 	import _fetch from "../../pe-fest-module/views/"
// 	import Login from "../../pe-fest-module/views/Login"
// 	import Token from "../../pe-fest-module/views/Token"
// 	import { setCookie, getCookie, deleteCookie } from "../../pe-fest-module/views/utilities/utils"
// }

class TutorRemoteAccountAdmin extends BasicState {// basic_state_data() {
  // 	let len = 0
  // 	for (let i = 0; i < 10; i++) {
  // 		const token = getCookie(`wpfest_token${i}`)
  // 		if (token) {
  // 			len++
  // 		}
  // 	}
  // 	return {
  // 		routing: "Login",
  // 		isCreateOpen: false,
  // 		len,
  // 	}
  // }
  // getRoute = () => "admin-fests"
  // addRender() {
  // 	const tokens = []
  // 	for (let i = 0; i < 10; i++) {
  // 		const token = getCookie(`wpfest_token${i}`)
  // 		// console.log(token);
  // 		if (token) {
  // 			tokens.push(
  // 				<Token
  // 					key={i}
  // 					i={i + 1}
  // 					token={token}
  // 					onLogout={this.onLogout}
  // 				/>,
  // 			)
  // 		}
  // 	}
  // 	return (
  // 		<div className="w-100">
  // 			{tokens}
  // 			<div className="my-1">
  // 				<Popover
  // 					position={Position.RIGHT_TOP}
  // 					isOpen={this.state.isCreateOpen}
  // 					content={(
  // 						<Login
  // 							onLogin={this.onLogin}
  // 						/>
  // 					)}
  // 				>
  // 					<div className="btn btn-light  btn-sm mt-2" onClick={this.onCreateToggle}>
  // 						<i className="fas fa-plus" />
  // 					</div>
  // 				</Popover>
  // 			</div>
  // 		</div>
  // 	)
  // }
  // onCreateToggle = () => {
  // 	this.setState({ isCreateOpen: !this.state.isCreateOpen })
  // }
  // onLogin = (data, url) => {
  // 	console.log(data, this.state.len)
  // 	AppToaster.show({
  // 		intent: Intent.SUCCESS,
  // 		icon: "tick",
  // 		message: __(data.msg),
  // 	})
  // 	if (data.token) {
  // 		setCookie(`wpfest_token${this.state.len}`, data.token)
  // 		setCookie(`wpfest_display_name${this.state.len}`, data.user.display_name)
  // 		setCookie(`wpfest_url${this.state.len}`, url)
  // 		this.setState({ len: this.state.len + 1, isCreateOpen: false })
  // 	}
  // }
  // onLogout = (data) => {
  // 	console.log(data - 1)
  // 	deleteCookie(`wpfest_token${data - 1}`)
  // 	let len = 0
  // 	for (let i = 0; i < 10; i++) {
  // 		const token = getCookie(`wpfest_token${i}`)
  // 		if (token) {
  // 			len++
  // 		}
  // 	}
  // 	this.setState({ len })
  // }
}

export default compose(withRouter)(TutorRemoteAccountAdmin);