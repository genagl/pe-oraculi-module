import React from "react";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { Query, withApollo } from "react-apollo";
import { __ } from "react-pe-utilities";
import BasicState from "react-pe-basic-view";
import { getQueryArgs, querySingleName, querySingle } from "react-pe-layouts";
import { Loading } from 'react-pe-useful';
import { initArea } from "react-pe-utilities";

class TutorResult extends BasicState {
  render() {
    const query_name = querySingleName("Bio_ImageResult");
    const query_args = getQueryArgs("Bio_ImageResult");
    const query = querySingle("Bio_ImageResult", query_name, query_args);
    const {
      id
    } = this.props.match.params;
    return /*#__PURE__*/React.createElement("div", {
      className: "layout-state p-0"
    }, /*#__PURE__*/React.createElement("div", {
      className: "position-relative "
    }, /*#__PURE__*/React.createElement(Query, {
      query: query,
      variables: {
        id
      }
    }, ({
      loading,
      error,
      data,
      client
    }) => {
      if (loading) {
        return /*#__PURE__*/React.createElement("div", {
          className: "layout-state bg-white"
        }, /*#__PURE__*/React.createElement(Loading, null));
      }

      if (data) {
        //console.log(data.getBio_ImageResult)
        const result = data.getBio_ImageResult ? data.getBio_ImageResult : {};
        return /*#__PURE__*/React.createElement("div", {
          className: " mt-5"
        }, /*#__PURE__*/React.createElement("div", {
          className: "tutor-row"
        }, /*#__PURE__*/React.createElement("div", {
          className: "tutor-left-aside  mobile-relatived"
        }, initArea("single-result-left-aside", { ...result,
          user: this.props.user
        })), /*#__PURE__*/React.createElement("div", {
          className: "tutor-main"
        }, /*#__PURE__*/React.createElement("div", {
          className: "sub-title text-center"
        }, result.post_title), /*#__PURE__*/React.createElement("div", {
          className: "d-flex justify-content-center mb-4"
        }, /*#__PURE__*/React.createElement("img", {
          src: result.thumbnail,
          style: {
            maxWidth: "100%",
            maxHeight: 500
          }
        })), /*#__PURE__*/React.createElement("div", {
          className: "r-course-underline",
          dangerouslySetInnerHTML: {
            __html: result.post_content
          }
        }), initArea("single_result", { ...result,
          user: this.props.user
        })), /*#__PURE__*/React.createElement("div", {
          className: "tutor-right-aside"
        }, initArea("single-result-right-aside", { ...result,
          user: this.props.user
        }))));
      }
    })));
  }

}

export default compose(withApollo, withRouter)(TutorResult);