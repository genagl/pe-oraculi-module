function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { Query, withApollo } from "react-apollo";
import gql from "graphql-tag";
import Test from "./tutorTest/Test";
import { __ } from "react-pe-utilities";
import BasicState from "react-pe-basic-view";
import { Loading } from 'react-pe-useful';
import { initArea } from "react-pe-utilities";
const query = gql`query getBio_Test($id:String) {
			getBio_Test(id:$id) { 
				id
				post_title
				post_content
				post_date
				is_timed
				duration
				try_count
				is_show_results
				is_shuffle
				is_show_answer
				is_group
				publish_type
				post_author
				{
				  id
				  display_name
				  avatar
				}
				bio_course
				{
				  id
				  post_title
				  logotype
				  children
				  {
					  id
					  post_title					  
					  children
					  {
						  id
						  post_title
						  
					  }
				  }
				}
				bio_olimpiad_type
				{
				  id
				  post_title
				}
				bio_class
				{
				  id
				  post_title
				}
				bio_biology_theme
				{
				  id
				  post_title
				  thumbnail
				  icon
				}
				comments
				{
					id
					discussion_id
					discussion_type
					content
					parent_id
					author
					{
						avatar
						display_name
						id
					}
					date
					is_approved					
				}				
				questions
				{
					id
					post_title
					post_content
					image_id
					thumbnail
					penalty
					type
					answers
					{		
						id
						post_content
						order
					}
				}
				__typename
			  }
		  }
		  `;

class TutorTest extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "getRoute", () => "course-lessons");

    _defineProperty(this, "onRestart", () => {// this.setState(
    });
  }

  render() {
    const {
      id
    } = this.props.match.params;
    return /*#__PURE__*/React.createElement("div", {
      className: "layout-state"
    }, /*#__PURE__*/React.createElement(Query, {
      query: query,
      variables: {
        id
      }
    }, ({
      loading,
      error,
      data,
      client
    }) => {
      if (loading) {
        return /*#__PURE__*/React.createElement("div", {
          className: "d-flex h-100 justify-content-center align-items-center"
        }, /*#__PURE__*/React.createElement(Loading, null));
      }

      if (data) {
        console.log(data.getBio_Test);
        const test = data.getBio_Test || {};
        const classes = test.bio_class.map((e, i) => /*#__PURE__*/React.createElement("div", {
          className: "tutor-label-class",
          key: i
        }, `${e.post_title} ${__("class")}`));
        const themes = test.bio_biology_theme.map((e, i) => /*#__PURE__*/React.createElement("div", {
          className: "tutor-label",
          key: i
        }, e.post_title));
        const courses = test.bio_course.map((e, i) => /*#__PURE__*/React.createElement("div", {
          className: "tutor-label-course",
          key: i
        }, e.post_title));
        const bio_olimpiad_type = test.bio_olimpiad_type.map((e, i) => /*#__PURE__*/React.createElement("div", {
          className: "tutor-label-olympiad_type",
          key: i
        }, e.post_title));
        const duration = test.is_show_answer ? test.duration + 4 * test.questions.length : test.duration;
        const leftClass = this.state.isLeftClosed ? "tutor-left-aside closed mobile-relatived" : "tutor-left-aside mobile-relatived ";
        const mainClass = this.state.isLeftClosed ? "tutor-main  opened" : "tutor-main ";
        return /*#__PURE__*/React.createElement("div", {
          className: "mt-5"
        }, /*#__PURE__*/React.createElement("div", {
          className: "tutor-row"
        }, /*#__PURE__*/React.createElement("div", {
          className: leftClass
        }, /*#__PURE__*/React.createElement("div", null, initArea("single-test-left-aside", { ...test,
          user: this.props.user,
          prefix: "tests"
        }))), /*#__PURE__*/React.createElement("div", {
          className: mainClass
        }, /*#__PURE__*/React.createElement("div", {
          className: "clapan-left hidden",
          onClick: () => this.setState({
            isLeftClosed: !this.state.isLeftClosed
          })
        }, /*#__PURE__*/React.createElement("div", {
          className: `fas fa-caret-${!this.state.isLeftClosed ? "left" : "right"}`
        })), /*#__PURE__*/React.createElement(Test, _extends({}, test, {
          duration: duration,
          user: this.props.user,
          onRestart: this.onRestart,
          query: query,
          variables: {
            id
          }
        })), initArea("single-test", { ...test,
          user: this.props.user,
          prefix: "tests"
        })), /*#__PURE__*/React.createElement("div", {
          className: "tutor-right-aside"
        }, initArea("single-test-right-aside", { ...test,
          user: this.props.user,
          prefix: "tests"
        }))));
      }

      if (error) {
        return error.toString();
      }
    }));
  }

}

export default compose(withApollo, withRouter)(TutorTest);