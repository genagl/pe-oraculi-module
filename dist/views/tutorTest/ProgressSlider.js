import React, { Component } from "react";
import $ from "jquery";
import { __ } from "react-pe-utilities";

class ProgressSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      progress: this.props.progress
    };
  }

  componentWillUpdate(nextProps) {
    if (nextProps.progress !== this.state.progress) {
      this.setState({
        progress: nextProps.progress
      });
    }
  }

  render() {
    const pw = $(".progress-container").width() || 0;
    const width = this.state.progress < this.props.full ? this.state.progress / this.props.full * 100 : 100;
    return /*#__PURE__*/React.createElement("div", {
      className: "progress-container mb-2"
    }, /*#__PURE__*/React.createElement("div", {
      className: "progress-slider",
      style: {
        width: `${width}%`
      }
    }), /*#__PURE__*/React.createElement("div", {
      style: {
        left: pw * this.state.progress / this.props.full - 10
      },
      className: "progress-slider-label"
    }, this.props.progress < this.props.full ? this.props.progress : __("Finish")));
  }

}

export default ProgressSlider;