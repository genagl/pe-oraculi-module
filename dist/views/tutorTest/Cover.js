import React, { Component } from "react";
import { Intent, Button, Icon } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";

class Cover extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const duration = this.props.duration ? `${this.props.duration} ${__("sec.")}` : "No limit";
    const try_count = !this.props.try_count || this.props.try_count == 0 ? __("only ones") : this.props.try_count + __(" counts");
    return /*#__PURE__*/React.createElement("div", {
      className: "test-cover"
    }, /*#__PURE__*/React.createElement("div", {
      className: "test-title",
      dangerouslySetInnerHTML: {
        __html: this.props.post_title
      }
    }), /*#__PURE__*/React.createElement("div", {
      className: "test-descr"
    }, /*#__PURE__*/React.createElement("div", {
      className: "row my-3"
    }, /*#__PURE__*/React.createElement("div", {
      className: "test-descr-title col-md-6"
    }, __("Time limit")), /*#__PURE__*/React.createElement("div", {
      className: "test-descr-cont col-md-6"
    }, duration), /*#__PURE__*/React.createElement("div", {
      className: "test-descr-title col-md-6"
    }, __("Questions count")), /*#__PURE__*/React.createElement("div", {
      className: "test-descr-cont col-md-6"
    }, this.props.questions.length), /*#__PURE__*/React.createElement("div", {
      className: "test-descr-title col-md-6"
    }, __("Show right answer after choose")), /*#__PURE__*/React.createElement("div", {
      className: "test-descr-cont col-md-6"
    }, /*#__PURE__*/React.createElement(Icon, {
      icon: this.props.is_show_answer ? "tick" : "cross",
      intent: this.props.is_show_answer ? Intent.SUCCESS : Intent.DANGER
    })), /*#__PURE__*/React.createElement("div", {
      className: "test-descr-title col-md-6"
    }, __("Show result after test")), /*#__PURE__*/React.createElement("div", {
      className: "test-descr-cont col-md-6"
    }, /*#__PURE__*/React.createElement(Icon, {
      icon: this.props.is_show_results ? "tick" : "cross",
      intent: this.props.is_show_results ? Intent.SUCCESS : Intent.DANGER
    })), /*#__PURE__*/React.createElement("div", {
      className: "test-descr-title col-md-6"
    }, __("Try count")), /*#__PURE__*/React.createElement("div", {
      className: "test-descr-cont col-md-6"
    }, try_count))), /*#__PURE__*/React.createElement("div", {
      className: "test-content",
      dangerouslySetInnerHTML: {
        __html: this.props.post_content
      }
    }), /*#__PURE__*/React.createElement("div", {
      className: ""
    }, /*#__PURE__*/React.createElement(Button, {
      onClick: this.props.step
    }, __("Start Test"))));
  }

}

export default Cover;