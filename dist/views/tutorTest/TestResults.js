function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Collapse, Button } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import { Loading } from 'react-pe-useful';
import QuestionTestResults from "./QuestionTestResults";

class TestResults extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "handleOpen", () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    });

    this.state = {
      isOpen: false
    };
  }

  render() {
    if (this.props.results == undefined) {
      return /*#__PURE__*/React.createElement(Loading, null);
    }

    const questions = this.props.questions.map((e, i) => {
      const result = this.props.results.questions.filter(ee => e.id == ee.id);
      return /*#__PURE__*/React.createElement(QuestionTestResults, {
        key: i,
        origin: e,
        result: result[0]
      });
    });
    return /*#__PURE__*/React.createElement("div", {
      className: "d-flex justify-content-center flex-column"
    }, /*#__PURE__*/React.createElement("div", {
      className: "test-result-data row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "test-descr-title col-md-6"
    }, __("Right count"), ":"), /*#__PURE__*/React.createElement("div", {
      className: "test-descr-cont col-md-6"
    }, this.props.results.right_count)), /*#__PURE__*/React.createElement("div", {
      className: "test-result-data row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "test-descr-title col-md-6"
    }, __("Credits"), ":"), /*#__PURE__*/React.createElement("div", {
      className: "test-descr-cont col-md-6"
    }, this.props.results.credits)), /*#__PURE__*/React.createElement("div", {
      className: "show-test-results"
    }, /*#__PURE__*/React.createElement(Button, {
      onClick: this.handleOpen,
      minimal: true,
      className: "small ",
      rightIcon: "chevron-down",
      fill: true
    }, __(this.state.isOpen ? "Hide all results" : "Show all results")), /*#__PURE__*/React.createElement(Collapse, {
      isOpen: this.state.isOpen,
      transitionDuration: 2000,
      className: ""
    }, /*#__PURE__*/React.createElement("div", {
      className: "px-4 py-3"
    }, questions))));
  }

}

export default TestResults;