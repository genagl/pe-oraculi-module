function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";

class AnswerCheckbox extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onChange", () => {
      this.props.onChange(this.props.id);
      this.timer = setTimeout(() => {// this.props.step();
      }, 7300);
    });

    this.state = {
      checked: this.props.checked
    };
  }

  componentWillUpdate(nextProps) {
    if (nextProps.checked !== this.state.checked) {
      if (this.state.checked) clearTimeout(this.timer);
      this.setState({
        checked: nextProps.checked
      });
    }

    if (nextProps.is_right !== this.state.is_right) {
      if (nextProps.is_right) clearTimeout(this.timer);
      this.setState({
        is_right: nextProps.is_right
      });
    }
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  render() {
    const ainmaClass = this.state.checked ? "pause-circle" // "pause-circle active"
    : "pause-circle";
    const rightClass = this.state.is_right ? "text-success text-bold" : "";
    const checkClass = !this.state.is_right ? "_lcheck_  " : "_lcheck_";
    return /*#__PURE__*/React.createElement("div", {
      className: "answer"
    }, /*#__PURE__*/React.createElement("label", {
      className: checkClass,
      htmlFor: this.props.id
    }, /*#__PURE__*/React.createElement("input", {
      type: "checkbox",
      id: this.props.id,
      name: this.props.name,
      value: this.props.value,
      checked: this.state.checked,
      onChange: this.onChange
    }), /*#__PURE__*/React.createElement("span", {
      className: rightClass
    }, this.props.post_content)), /*#__PURE__*/React.createElement("div", {
      className: ainmaClass
    }, /*#__PURE__*/React.createElement("svg", {
      viewBox: "0 0 100 100",
      xmlns: "http://www.w3.org/2000/svg"
    }, /*#__PURE__*/React.createElement("circle", {
      cx: "50",
      cy: "50",
      r: "47"
    }))));
  }

}

export default AnswerCheckbox;