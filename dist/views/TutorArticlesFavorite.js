function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { Query, withApollo } from "react-apollo";
import gql from "graphql-tag";
import BasicState from "react-pe-basic-view";
import { Loading } from 'react-pe-useful';
import { initArea } from "react-pe-utilities";
import LessonQuote from "./tutor/LessonQuote";

class TutorArticlesFavorite extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "getRoute", () => "favorites");
  }

  addRender() {
    const query = gql`query getBio_Articles  
		{
			getBio_Articles(
				paging:{
					 metas:{key:"is_favorite", value:"1"}
				  }
			)
			{				
				id
				post_title
				post_content
				post_date
				thumbnail
				post_author
				{
					id
					display_name
				}
				bio_course
				{
					id
					post_title
				}
				bio_olimpiad_type
				{
					id
					post_title
				}
				bio_class
				{
					id
					post_title
				}
				bio_biology_theme
				{
					id
					post_title
					thumbnail
				}
				is_favorite
			}
		}`;
    return /*#__PURE__*/React.createElement("div", {
      className: "layout-state p-0"
    }, /*#__PURE__*/React.createElement(Query, {
      query: query
    }, ({
      loading,
      error,
      data,
      client
    }) => {
      if (loading) {
        return /*#__PURE__*/React.createElement(Loading, null);
      }

      if (data) {
        console.log(data.getBio_Articles);
        const articles = data.getBio_Articles || [];
        const favorites = articles.map((e, i) => /*#__PURE__*/React.createElement(LessonQuote, _extends({}, e, {
          key: i
        })));
        return /*#__PURE__*/React.createElement("div", {
          className: " mt-5"
        }, /*#__PURE__*/React.createElement("div", {
          className: "tutor-row"
        }, /*#__PURE__*/React.createElement("div", {
          className: "tutor-left-aside mobile-relatived"
        }, initArea("user-favorites-left-aside", { ...this.props
        })), /*#__PURE__*/React.createElement("div", {
          className: "tutor-main"
        }, favorites, initArea("user-favorites", { ...this.props
        })), /*#__PURE__*/React.createElement("div", {
          className: "tutor-right-aside"
        }, initArea("user-favorites-right-aside", { ...this.props
        }))));
      }

      if (error) {
        return error.toString();
      }
    }));
  }

}

export default compose(withApollo, withRouter)(TutorArticlesFavorite);