function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import gql from "graphql-tag";
import { __ } from "react-pe-utilities";

class EventsRequestLabelWidget extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      col: 0
    });
  }

  componentDidMount() {
    if (this.props.data.data_type == "Bio_Event") {
      const query = gql`
				query getFullEventMembersCount
				{
					getFullEventMembersCount
				}
			`;
      this.props.client.query({
        query
      }).then(result => {
        console.log(result);
        this.setState({
          col: result.data.getFullEventMembersCount
        });
      });
    }
  }

  render() {
    // console.log( this.props.data );
    switch (this.props.data.data_type) {
      case "Bio_Event":
        return /*#__PURE__*/React.createElement(React.Fragment, null, this.state.col > 0 ? /*#__PURE__*/React.createElement("div", {
          className: "menu-labla hint hint--left",
          "data-hint": __("Requests")
        }, this.state.col) : null);

      default:
        return null;
    }
  }

}

export default compose(withApollo)(EventsRequestLabelWidget);