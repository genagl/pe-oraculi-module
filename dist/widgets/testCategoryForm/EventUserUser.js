function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import gql from "graphql-tag";
import { Intent, Button, ButtonGroup } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";

class EventUserUser extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onAccept", () => {
      /*
      if(this.props.onAccept)
        this.props.onAccept();
      return;
      */
      const user_id = this.props.id;
      const {
        event_id
      } = this.props;
      const mutation = gql`
			mutation Accept_Request
			{
			  Accept_Request(input:{
				user:${user_id},
				event: ${event_id}
			  })
			}
		`;
      this.props.client.mutate({
        mutation
      }).then(result => {
        console.log(result);
        if (this.props.onAccept) this.props.onAccept();
      });
    });

    _defineProperty(this, "onRefuse", () => {
      if (this.props.onRefuse) this.props.onRefuse();
    });
  }

  render() {
    const {
      display_name,
      user_email,
      id,
      event_id
    } = this.props;
    return /*#__PURE__*/React.createElement("div", {
      className: "row mx-1"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-4 py-1 border-bottom border-secondary"
    }, display_name), /*#__PURE__*/React.createElement("div", {
      className: "col-md-4 py-1 border-bottom border-secondary"
    }, user_email), /*#__PURE__*/React.createElement("div", {
      className: "col-md-4 py-1 border-bottom border-secondary"
    }, /*#__PURE__*/React.createElement(ButtonGroup, null, /*#__PURE__*/React.createElement(Button, {
      intent: Intent.SUCCESS,
      onClick: this.onAccept
    }, __("Agree")), /*#__PURE__*/React.createElement(Button, {
      intent: Intent.DANGER,
      onClick: this.onRefuse
    }, __("Refuse")))));
  }

}

export default compose(withApollo)(EventUserUser);