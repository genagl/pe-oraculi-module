function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import gql from "graphql-tag";
import $ from "jquery";
import { Button } from "@blueprintjs/core";
import Question from "./Question";
import { Loading } from 'react-pe-useful';
import { __ } from "react-pe-utilities";

class TestQuestions extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onSwitchUp", data => {
      const questions = [...this.state.questions];
      this.state.questions.forEach((e, i) => {
        if (e.id === data) {
          const me = questions.splice(i, 1);
          questions.splice(i - 1, 0, me[0]);
        }
      });
      $(".question").removeClass("active");
      this.setState({
        questions,
        selected: data
      }, () => $(`#${data}`).addClass("active"));
      if (this.props.onChange) this.props.onChange(["questions", questions.filter(e => !e.isNew).map(e => {
        delete e.saved;
        return e;
      })]);
    });

    _defineProperty(this, "onSwitchDn", data => {
      const questions = [...this.state.questions];
      this.state.questions.forEach((e, i) => {
        if (e.id === data) {
          const me = questions.splice(i, 1);
          questions.splice(i + 1, 0, me[0]);
        }
      });
      $(".question").removeClass("active");
      this.setState({
        questions,
        selected: data
      }, () => $(`#${data}`).addClass("active"));
      if (this.props.onChange) this.props.onChange(["questions", questions.filter(e => !e.isNew).map(e => {
        delete e.saved;
        return e;
      })]);
    });

    _defineProperty(this, "onInsertQuestion", () => {
      const questions = [...this.state.questions];
      const newId = this.state.questions.length.toString();
      questions.push({
        post_title: "",
        id: newId,
        saved: false,
        isNew: true
      });
      $(".question").removeClass("active");
      this.setState({
        questions,
        selected: newId
      });
      if (this.props.onChange) this.props.onChange(["questions", questions.filter(e => !e.isNew).map(e => {
        delete e.saved;
        return e;
      })]);
    });

    _defineProperty(this, "onAddQuestion", data => {
      const questions = [...this.state.questions];
      const newId = this.state.questions.length.toString();
      this.state.questions.forEach((e, i) => {
        if (e.id === data) {
          const me = {
            post_title: "",
            id: newId,
            saved: false,
            isNew: true
          };
          questions.splice(i + 1, 0, me);
        }
      });
      $(".question").removeClass("active");
      this.setState({
        questions,
        selected: newId
      });
      if (this.props.onChange) this.props.onChange(["questions", questions.filter(e => !e.isNew).map(e => {
        delete e.saved;
        return e;
      })]);
    });

    _defineProperty(this, "onSwitchOrder", data => {
      const questions = [...this.state.questions];
      this.state.questions.forEach((e, i) => {
        if (e.id === data[0]) {
          const me = questions.splice(i, 1);
          questions.splice(data[1], 0, me[0]);
        }
      });
      $(".question").removeClass("active");
      this.setState({
        questions,
        selected: data[0]
      }, () => $(`#${data[0]}`).addClass("active"));
      if (this.props.onChange) this.props.onChange(["questions", questions.filter(e => !e.isNew).map(e => {
        delete e.saved;
        return e;
      })]);
    });

    _defineProperty(this, "changeQuestion", data => {
      const questions = [...this.state.questions];
      let me;
      this.state.questions.forEach((e, i) => {
        if (e.id === data[0]) {
          me = questions.splice(i, 1, data[1]);
        }
      }); // console.log("added:", 	[ ...this.state.added, data[1] ] );

      const deleted = me[0].isNew ? this.state.deleted : [...this.state.deleted, me[0]];
      const added = [...this.state.added, data[1]];
      $(".question").removeClass("active");
      this.setState({
        questions,
        selected: data[1].id,
        deleted,
        added
      }, () => $(`#${data[1].id}`).addClass("active"));
      if (this.props.onChange) this.props.onChange(["questions", questions.filter(e => !e.isNew).map(e => {
        delete e.saved;
        return e;
      })]);
    });

    _defineProperty(this, "onDelete", data => {
      const questions = [...this.state.questions];
      let me;
      this.state.questions.forEach((e, i) => {
        if (e.id === data) {
          me = questions.splice(i, 1);
        }
      });
      $(".question").removeClass("active");
      this.setState({
        questions,
        selected: -1,
        deleted: me[0].isNew ? this.state.deleted : [...this.state.deleted, me[0]]
      });
      if (this.props.onChange) this.props.onChange(["questions", questions.filter(e => !e.isNew).map(e => {
        delete e.saved;
        return e;
      })]);
    });

    this.state = {
      questions: [],
      selected: null,
      loading: true,
      added: [],
      deleted: []
    };
  }

  componentDidMount() {
    const test_id = `"${this.props.id}"`;
    const query = gql`
			query
			{
			  getBio_Test( id: ${test_id} )
			  {
				id
				questions
				{
				  id
				  post_title 
				  thumbnail
				  hidden
				  penalty
				  single
				  is_deleted
				  order
				  bio_biology_theme
				  {
					  id
					  post_title
				  }
				  bio_test
				  {
					  id
					  post_title					  
				  }
				  answers
				  {
					  post_content
					  id
					  order
				  }
				}
			  }
			}
		`;
    this.props.client.query({
      query,
      variables: {
        id: test_id
      }
    }).then(result => {
      // console.log( result.data.getBio_Test.questions );
      this.setState({
        questions: result.data.getBio_Test.questions,
        loading: false
      });
    });
  }

  render() {
    if (this.state.loading) return /*#__PURE__*/React.createElement(Loading, null); // console.log("added:", 	this.state.added);
    // console.log("deleted:", this.state.deleted);

    const questions = this.state.questions.map((e, i) => /*#__PURE__*/React.createElement(Question, _extends({
      key: i
    }, e, {
      i: i,
      is_first: i === 0,
      is_last: i === this.state.questions.length - 1,
      selected: this.state.selected === e.id,
      onSwitchUp: this.onSwitchUp,
      onSwitchDn: this.onSwitchDn,
      onSwitchOrder: this.onSwitchOrder,
      onAddQuestion: this.onAddQuestion,
      changeQuestion: this.changeQuestion,
      onDelete: this.onDelete
    })));
    return /*#__PURE__*/React.createElement("div", {
      className: "row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-3"
    }), /*#__PURE__*/React.createElement("div", {
      className: "col-md-9"
    }, /*#__PURE__*/React.createElement(Link, {
      to: this.props.parent_route,
      className: "btn btn-link"
    }, __("edit Test"))), /*#__PURE__*/React.createElement("div", {
      className: "col-md-3"
    }), /*#__PURE__*/React.createElement("div", {
      className: "col-md-9 "
    }, questions), /*#__PURE__*/React.createElement("div", {
      className: "col-md-3"
    }), /*#__PURE__*/React.createElement("div", {
      className: "col-md-9 "
    }, /*#__PURE__*/React.createElement(Button, {
      className: "ml-2 my-2",
      onClick: this.onInsertQuestion,
      icon: "plus"
    })));
  }

}

export default compose(withApollo, withRouter)(TestQuestions);