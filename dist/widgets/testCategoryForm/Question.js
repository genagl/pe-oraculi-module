function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import $ from "jquery";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import gql from "graphql-tag";
import { Icon, Tag, Intent, PopoverInteractionKind, Dialog, Button, ButtonGroup, Position, Popover } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import { AppToaster } from 'react-pe-useful';

class Question extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onSelect", evt => {
      $(".question").removeClass("active");
      evt.currentTarget.classList.add("active");
    });

    _defineProperty(this, "onSwitchOrder", evt => {
      // console.log( this.state.id );
      this.props.onSwitchOrder([this.props.id, evt.currentTarget.value]);
    });

    _defineProperty(this, "onAddQuestion", evt => {
      // console.log( this.state.id );
      this.props.onAddQuestion(this.state.id);
    });

    _defineProperty(this, "onChQ", () => {
      this.setState({
        isChQ: !this.state.isChQ
      });
    });

    _defineProperty(this, "onOpenQuestion", () => {
      this.setState({
        isChQOpen: !this.state.isChQOpen
      });
    });

    _defineProperty(this, "onChQuestion", () => {
      this.setState({
        isChQOpen: true
      });
    });

    _defineProperty(this, "onDlQuestion", () => {
      this.setState({
        isChQ: true,
        dialogTitle: __("Delete Question"),
        dialogContent: /*#__PURE__*/React.createElement("div", {
          className: "p-5"
        }, /*#__PURE__*/React.createElement("div", {
          className: "btn btn-danger",
          onClick: this.onDelete
        }, __("Delete this Question?")))
      });
    });

    _defineProperty(this, "onDelete", () => {
      this.props.onDelete(this.state.id);
      this.setState({
        isChQ: false
      });
    });

    _defineProperty(this, "searchList", () => {
      const list = this.state.search.map((e, i) => /*#__PURE__*/React.createElement(Button, {
        fill: true,
        key: i,
        qid: e.id,
        onClick: this.onSelectNewQuestion
      }, e.post_title));
      return this.state.search.length > 0 ? /*#__PURE__*/React.createElement(ButtonGroup, {
        vertical: true
      }, list) : /*#__PURE__*/React.createElement("div", {
        className: "alert alert-danger"
      }, __("Search result is empty"));
    });

    _defineProperty(this, "onChangeValue", evt => {
      this.setState({
        value: evt.currentTarget.value
      });
    });

    _defineProperty(this, "onSelectNewQuestion", evt => {
      const qid = evt.currentTarget.getAttribute("qid");
      const srch = this.state.search.filter(e => e.id === qid)[0];
      this.setState({
        result: srch,
        isSearchOpen: false
      });
    });

    _defineProperty(this, "onSearch", () => {
      // console.log(this.state.value);
      if (this.state.value && this.state.value.length > 0) {
        const _search = this.state.value;
        const query = gql`
			query
			{
				getBio_TestQuestions(
					paging:
					{
						offset:0, 
						count:50,
					  search:"${_search}"
					}
			  )
			  {
					id
					post_title
					bio_test
					{
					  id
					  post_title
					  
					}
					bio_biology_theme
					{
					  id
					  post_title
					}
			  }
			}`;
        this.props.client.query({
          query
        }).then(result => {
          console.log(result.data.getBio_TestQuestions); // console.log( this.state );

          this.setState({
            search: result.data.getBio_TestQuestions,
            dialogContent: this.changeIF(),
            isSearchOpen: !this.state.isSearchOpen
          });
        });
      } else {
        AppToaster.show({
          intent: Intent.DANGER,
          icon: "tick",
          message: "Insert not empty title"
        });
      }
    });

    _defineProperty(this, "changeQuestion", () => {
      this.setState({
        isChQOpen: false
      });
      this.props.changeQuestion([this.state.id, { ...this.state.result,
        saved: false
      }]);
    });

    this.state = { ...this.props,
      value: "",
      dialogTitle: __("Dialog title"),
      dialogContent: __("Dialog Content"),
      search: [],
      saved: typeof props.saved == "undefined" ? true : props.saved,
      result: {
        post_title: props.post_title,
        id: props.id
      }
    };
  }

  componentDidMount() {}

  componntDidUnmount() {}

  componentWillReceiveProps(nextProps) {
    this.setState({ ...nextProps,
      saved: typeof nextProps.saved == "undefined" ? true : nextProps.saved
    });
  }

  render() {
    console.log(this.state);
    const saved = this.state.saved ? /*#__PURE__*/React.createElement(Icon, {
      icon: "tick",
      intent: Intent.SUCCESS
    }) : /*#__PURE__*/React.createElement(Icon, {
      icon: "error",
      intent: Intent.DANGER
    });
    return /*#__PURE__*/React.createElement("div", {
      className: `row data-list question ${this.state.selected ? "active" : ""}`,
      id: this.state.id,
      onMouseDown: this.onSelect
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-1 py-2"
    }, /*#__PURE__*/React.createElement(ButtonGroup, {
      vertical: true
    }, /*#__PURE__*/React.createElement(Button, {
      icon: "caret-up",
      minimal: true,
      onClick: () => this.props.onSwitchUp(this.state.id),
      disabled: this.props.is_first
    }), /*#__PURE__*/React.createElement("input", {
      type: "number",
      className: "input dark",
      value: this.state.i,
      style: {
        width: 50,
        textAlign: "center"
      },
      onChange: this.onSwitchOrder
    }), /*#__PURE__*/React.createElement(Button, {
      icon: "caret-down",
      minimal: true,
      onClick: () => this.props.onSwitchDn(this.props.id),
      disabled: this.props.is_last
    }))), /*#__PURE__*/React.createElement("div", {
      className: "col-9 py-2"
    }, this.state.post_title ? /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
      dangerouslySetInnerHTML: {
        __html: this.state.post_title
      }
    }), /*#__PURE__*/React.createElement("div", null, this.state.bio_biology_theme ? /*#__PURE__*/React.createElement(Tag, {
      style: {
        backgroundColor: "#8a497f"
      },
      className: "mt-2"
    }, this.state.bio_biology_theme.post_title) : null), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
      className: "text-secondary mt-2"
    }, __("Exists in Tests:")), this.state.bio_test && Array.isArray(this.state.bio_test) ? this.state.bio_test.map((e, i) => /*#__PURE__*/React.createElement(Tag, {
      className: "m-1",
      minimal: true,
      intent: Intent.WARNING
    }, e.post_title)) : __("No exists"))) : /*#__PURE__*/React.createElement("div", {
      className: "btn btn-danger",
      onClick: this.onOpenQuestion
    }, __("Select Question"))), /*#__PURE__*/React.createElement("div", {
      className: "col-1 py-2"
    }, saved, /*#__PURE__*/React.createElement("div", {
      className: "hidden "
    }, this.state.id)), /*#__PURE__*/React.createElement("div", {
      className: "col-1 py-2"
    }, /*#__PURE__*/React.createElement(Popover, {
      interactionKind: PopoverInteractionKind.CLICK,
      popoverClassName: "bp3-popover-content-sizing",
      position: Position.LEFT,
      content: /*#__PURE__*/React.createElement(ButtonGroup, {
        vertical: true
      }, /*#__PURE__*/React.createElement(Button, {
        minimal: true,
        onClick: this.onOpenQuestion
      }, __("Change Question")), /*#__PURE__*/React.createElement(Button, {
        minimal: true,
        onClick: this.onAddQuestion
      }, __("Add Question after")), /*#__PURE__*/React.createElement(Button, {
        minimal: true,
        onClick: this.onDlQuestion
      }, __("Remove Question")))
    }, /*#__PURE__*/React.createElement(Button, {
      minimal: true
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-ellipsis-v"
    })))), /*#__PURE__*/React.createElement(Dialog, {
      isOpen: this.state.isChQ,
      onClose: this.onChQ,
      title: this.state.dialogTitle,
      className: "little"
    }, this.state.dialogContent), /*#__PURE__*/React.createElement(Dialog, {
      isOpen: this.state.isChQOpen,
      onClose: this.onOpenQuestion,
      title: __("Change Question"),
      className: "little"
    }, this.changeIF()));
  }

  changeIF() {
    const btn = this.state.id !== this.state.result.id ? /*#__PURE__*/React.createElement("div", {
      className: "btn btn-danger",
      onClick: this.changeQuestion
    }, __("Change Question")) : null;
    return /*#__PURE__*/React.createElement("div", {
      className: "p-5"
    }, /*#__PURE__*/React.createElement("div", {
      className: "pb-3 text-secondary"
    }, /*#__PURE__*/React.createElement("span", null, __("Current Question:")), /*#__PURE__*/React.createElement("span", {
      className: "title pl-3"
    }, this.state.result.post_title)), /*#__PURE__*/React.createElement("div", {
      className: "title"
    }, __("Select Question by substring")), /*#__PURE__*/React.createElement("div", {
      className: "input-group mb-3 position-relative"
    }, /*#__PURE__*/React.createElement("input", {
      type: "text",
      value: this.state.value,
      onChange: this.onChangeValue,
      className: "form-control dark input ",
      placeholder: __("Set substring for search")
    }), /*#__PURE__*/React.createElement("div", {
      className: "input-group-append"
    }, /*#__PURE__*/React.createElement(Popover, {
      isOpen: this.state.isSearchOpen,
      content: /*#__PURE__*/React.createElement("div", {
        className: "p-0 max-width-350 max-width-250  max-height-250 overflow-y-auto"
      }, /*#__PURE__*/React.createElement("div", {
        style: {
          overflowX: "hidden",
          overflowY: "auto",
          marginBottom: 10
        }
      }, this.searchList()))
    }, /*#__PURE__*/React.createElement("button", {
      className: "btn btn-outline-secondary",
      type: "button",
      onClick: this.onSearch
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-search"
    }))))), /*#__PURE__*/React.createElement("div", {
      className: "pt-3",
      style: {
        height: 50
      }
    }, btn));
  }

}

export default compose(withApollo)(Question);