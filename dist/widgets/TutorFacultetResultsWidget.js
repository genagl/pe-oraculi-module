import React, { Component, Suspense } from "react";
import { NavLink } from "react-router-dom";
import { __ } from "react-pe-utilities";
import { Loading } from 'react-pe-useful';
const OwlCarousel = /*#__PURE__*/React.lazy(() => import('react-owl-carousel2fix'));

class TutorFacultetResultsWidget extends Component {
  constructor(props) {
    super(props);
    this.car = /*#__PURE__*/React.createRef();
    this.po = /*#__PURE__*/React.createRef();
  }

  render() {
    if (this.props.bio_image_result.length == 0) return "";
    const options = {
      loop: true,
      autoplay: true,
      margin: 0,
      items: 1,
      responsive: {
        0: {
          items: 1
        },
        1000: {
          items: 1
        },
        1200: {
          items: 1
        }
      }
    };
    const bio_image_result = this.props.bio_image_result.map((e, i) => /*#__PURE__*/React.createElement(NavLink, {
      className: "w-100",
      key: i,
      to: `/result/${e.id}`
    }, /*#__PURE__*/React.createElement("div", {
      className: ""
    }, e.post_title), /*#__PURE__*/React.createElement("div", {
      style: {
        backgroundImage: `url(${e.thumbnail})`,
        width: "100%",
        height: 300,
        backgroundSize: "cover",
        backgroundPosition: "center"
      }
    })));
    const coursesList = /*#__PURE__*/React.createElement(Suspense, {
      fallback: /*#__PURE__*/React.createElement(Loading, null)
    }, /*#__PURE__*/React.createElement(OwlCarousel, {
      ref: this.car,
      options: options
    }, bio_image_result));
    return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
      className: "aside-widget-title"
    }, __("Our graduates results:")), coursesList);
  }

}

export default TutorFacultetResultsWidget;