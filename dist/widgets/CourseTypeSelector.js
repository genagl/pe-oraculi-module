function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Query, withApollo } from "react-apollo";
import { compose } from "recompose";
import { withRouter } from "react-router";
import { Loading } from 'react-pe-useful';
import { __ } from "react-pe-utilities";
import { getQueryArgs, getQueryName, queryCollection } from "react-pe-layouts";

class CourseTypeSelector extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      is_open: false
    });

    _defineProperty(this, "courseTypeSelector", (collection, id) => {
      let {
        course
      } = this.props;
      let category = {};

      if (course) {
        category = collection.filter(e => e.id === course.parent);
      } else {
        category = collection[0];
        course = collection[0];
      }

      const list = collection.map((e, i) => {
        const children = e.children && e.children.length > 0 ? e.children[0] : [];

        if (children.id) {
          return /*#__PURE__*/React.createElement("li", {
            key: i,
            className: "list-elem"
          }, /*#__PURE__*/React.createElement(NavLink, {
            to: `/course/${children.id}/posts`
          }, e.post_title));
        }

        return /*#__PURE__*/React.createElement("li", {
          key: i,
          className: "list-elem "
        }, /*#__PURE__*/React.createElement("a", null, e.post_title));
      });
      if (!category) return null;
      console.log(category);
      const children = category && category.children && category.children.length > 0 ? category.children : [];

      const _courses = children.map((e, i) => /*#__PURE__*/React.createElement("div", {
        className: `col-6 p-2 course-select-item ${e.id == course.id ? "active" : ""}`,
        course_id: e.id
      }, /*#__PURE__*/React.createElement(NavLink, {
        to: `/course/${e.id}/posts`,
        className: "course-item-alt "
      }, /*#__PURE__*/React.createElement("div", {
        className: "thrumb",
        style: {
          backgroundImage: `url(${e.thumbnail})`
        }
      }), /*#__PURE__*/React.createElement("div", {
        className: "title"
      }, e.post_title))));

      return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
        className: "course_category_select pointer",
        onClick: this.openToggle
      }, /*#__PURE__*/React.createElement("div", {
        className: "filter category"
      }, __(category ? category.post_title : "")), /*#__PURE__*/React.createElement("div", {
        className: !this.state.is_open ? "_arrow closed" : "_arrow opened"
      })), /*#__PURE__*/React.createElement("div", {
        className: "d-block"
      }, /*#__PURE__*/React.createElement("div", {
        className: "courses_list",
        id: `sel_${category.id}`,
        style: {
          height: this.state.is_open ? this.state.height : 0
        }
      }, /*#__PURE__*/React.createElement("ul", {
        className: "list"
      }, list))), /*#__PURE__*/React.createElement("div", {
        className: "col-12 borded"
      }, /*#__PURE__*/React.createElement("div", {
        className: "row"
      }, _courses)));
    });

    _defineProperty(this, "openToggle", evt => {
      this.setState({
        is_open: !this.state.is_open // height:$("#sel_"+this.props.course_group._id + " ul.list").height() + 15

      });
    });
  }

  render() {
    const query_name = getQueryName("Bio_Course");
    const query_args = getQueryArgs("Bio_Course");
    const query = queryCollection("Bio_Course", query_name, query_args);
    const {
      id
    } = this.props.match.params;
    return /*#__PURE__*/React.createElement(Query, {
      query: query
    }, ({
      loading,
      error,
      data,
      client
    }) => {
      if (loading) {
        return /*#__PURE__*/React.createElement(Loading, null);
      }

      if (data) {
        const courses = data[query_name].filter(e => e.parent === 0);
        return this.courseTypeSelector(courses, id);
      }

      if (error) {
        return error.toString();
      }
    });
  }

}

export default compose(withApollo, withRouter)(CourseTypeSelector);