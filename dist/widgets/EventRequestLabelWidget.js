import React, { Component } from "react";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import { __ } from "react-pe-utilities";

class EventRequestLabelWidget extends Component {
  render() {
    console.log(this.props.elem.request_count);
    return this.props.data_type == "Bio_Event" && this.props.data[0] == "edit" ? /*#__PURE__*/React.createElement("div", {
      className: "position-relative"
    }, this.props.defArea, this.props.elem.request_count > 0 ? /*#__PURE__*/React.createElement("div", {
      className: "labla",
      title: "Requests"
    }, this.props.elem.request_count) : null) : this.props.defArea;
  }

}

export default compose(withApollo, withRouter)(EventRequestLabelWidget);