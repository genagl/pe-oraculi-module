import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { __ } from "react-pe-utilities";

class TutorCurrentClassWidget extends Component {
  render() {
    if (!this.props.bio_class || this.props.bio_class.length === 0) return null;
    const courses = this.props.bio_class.map((e, i) => {
      const cl = "tutor-course-widget-cont";
      return /*#__PURE__*/React.createElement("div", {
        className: "tutor-course-widget-container",
        key: i
      }, /*#__PURE__*/React.createElement(NavLink, {
        className: cl,
        to: `/class-lessons/${e.id}/${this.props.prefix}`
      }, /*#__PURE__*/React.createElement("div", {
        className: "tutor-course-widget-logo",
        style: {
          backgroundImage: `url(${e.logotype})`
        }
      }), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
        className: "tutor-course-widget-title"
      }, `${e.post_title} ${__("class")}`))));
    });
    return /*#__PURE__*/React.createElement("div", {
      className: "course_category_select mb-5"
    }, /*#__PURE__*/React.createElement("div", {
      className: "aside-widget-title"
    }, __("Classes")), /*#__PURE__*/React.createElement("div", null, courses));
  }

}

export default TutorCurrentClassWidget;