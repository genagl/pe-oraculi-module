function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
правая панель на страницах курса, результатов поиска
 */
import React, { Component } from "react";
import { Query, withApollo } from "react-apollo";
import { compose } from "recompose";
import { withRouter } from "react-router";
import { __ } from "react-pe-utilities";
import { getQueryArgs, getQueryName, queryCollection } from "react-pe-layouts";
import { Loading } from 'react-pe-useful';

class CourseFilesWidget extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "rightPanel", (single, id) => {
      return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
        className: "course_category_select"
      }, /*#__PURE__*/React.createElement("div", {
        className: "filter category"
      }, __("Файлы курса:")), /*#__PURE__*/React.createElement("div", {
        className: "btn btn-primary"
      }, __("Скачать"))));
    });

    this.state = {};
  }

  render() {
    const query_name = getQueryName("Bio_course");
    const query_args = getQueryArgs("Bio_course");
    const query = queryCollection("Bio_course", query_name, query_args);
    const {
      id
    } = this.props.match.params;
    return /*#__PURE__*/React.createElement(Query, {
      query: query
    }, ({
      loading,
      error,
      data,
      client
    }) => {
      if (loading) {
        return /*#__PURE__*/React.createElement(Loading, null);
      }

      if (data) {
        console.log(data);
        const single = data[query_name][query_name][0] ? data[query_name][query_name][0] : [];
        return this.rightPanel(single, id);
      }

      if (error) {
        return error.toString();
      }
    });
  }

}

export default compose(withApollo, withRouter)(CourseFilesWidget);