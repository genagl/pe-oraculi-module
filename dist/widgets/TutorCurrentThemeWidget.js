import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { __ } from "react-pe-utilities";

class TutorCurrentThemeWidget extends Component {
  render() {
    if (!this.props.bio_biology_theme || this.props.bio_biology_theme.length === 0) return null;
    const courses = this.props.bio_biology_theme.map((e, i) => {
      const cl = e.children && e.children.length > 0 ? "tutor-course-widget-cont sub" : "tutor-course-widget-cont";
      const children = e.children ? e.children.map((ee, ii) => {
        const cl1 = ee.children.length > 0 ? "tutor-course-widget-cont sub" : "tutor-course-widget-cont";
        const children2 = ee.children.map((eee, iii) => /*#__PURE__*/React.createElement(NavLink, {
          className: "pl-4 tutor-course-widget-cont",
          key: iii,
          to: `/theme-lessons/${eee.id}/${this.props.prefix}`
        }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
          className: "tutor-course-widget-title2"
        }, `-- ${eee.post_title}`))));
        return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(NavLink, {
          className: cl1,
          key: ii,
          to: `/theme-lessons/${ee.id}/${this.props.prefix}`
        }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
          className: "tutor-course-widget-title2"
        }, `- ${ee.post_title}`))), children2);
      }) : null;
      return /*#__PURE__*/React.createElement("div", {
        className: "tutor-course-widget-container",
        key: i
      }, /*#__PURE__*/React.createElement(NavLink, {
        className: cl,
        to: `/theme-lessons/${e.id}/${this.props.prefix}`
      }, /*#__PURE__*/React.createElement("div", {
        className: "tutor-course-widget-logo",
        style: {
          backgroundImage: `url(${e.icon})`
        }
      }), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
        className: "tutor-course-widget-title"
      }, e.post_title))), children);
    });
    return /*#__PURE__*/React.createElement("div", {
      className: "course_category_select mb-5"
    }, /*#__PURE__*/React.createElement("div", {
      className: "aside-widget-title"
    }, __("Themes")), /*#__PURE__*/React.createElement("div", null, courses));
  }

}

export default TutorCurrentThemeWidget;