import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { __ } from "react-pe-utilities";

class TutorSubCoursesWidget extends Component {
  render() {
    if (!this.props.children || this.props.children.length === 0) return null;
    const courses = this.props.children.length > 0 ? this.props.children.map((e, i) => {
      const cl = e.children && e.children.length > 0 ? "tutor-course-widget-cont sub" : "tutor-course-widget-cont";
      const children = e.children ? e.children.map((ee, ii) => {
        const cl1 = ee.children.length > 0 ? "tutor-course-widget-cont sub" : "tutor-course-widget-cont";
        const children2 = ee.children.map((eee, iii) => /*#__PURE__*/React.createElement(NavLink, {
          className: "pl-4 tutor-course-widget-cont",
          key: iii,
          to: `/course-lessons/${eee.id}`
        }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
          className: "tutor-course-widget-title2"
        }, `-- ${eee.post_title}`))));
        return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(NavLink, {
          className: cl1,
          key: ii,
          to: `/course-lessons/${ee.id}`
        }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
          className: "tutor-course-widget-title2"
        }, `- ${ee.post_title}`))), children2);
      }) : null;
      return /*#__PURE__*/React.createElement("div", {
        className: "tutor-course-widget-container",
        key: i
      }, /*#__PURE__*/React.createElement(NavLink, {
        className: cl,
        to: `/course-lessons/${e.id}`
      }, /*#__PURE__*/React.createElement("div", {
        className: "tutor-course-widget-logo",
        style: {
          backgroundImage: `url(${e.logotype})`
        }
      }), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
        className: "tutor-course-widget-title"
      }, e.post_title))), children);
    }) : /*#__PURE__*/React.createElement("div", {
      className: "alert alert-secondary"
    }, __("No sub-courses exists"));
    return /*#__PURE__*/React.createElement("div", {
      className: "course_category_select mb-5"
    }, /*#__PURE__*/React.createElement("div", {
      className: "title"
    }, __("Sub Courses:")), /*#__PURE__*/React.createElement("div", null, courses));
  }

}

export default TutorSubCoursesWidget;