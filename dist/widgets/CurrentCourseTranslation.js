import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { __ } from "react-pe-utilities";

class CurrentCourseTranslation extends Component {
  render() {
    return /*#__PURE__*/React.createElement(NavLink, {
      className: "btn btn-light",
      to: "/translation/3290"
    }, __("Translation"));
  }

}

export default CurrentCourseTranslation;