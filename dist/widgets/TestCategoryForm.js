function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Route, Switch, withRouter } from "react-router";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import { __ } from "react-pe-utilities";
import TestQuestions from "./testCategoryForm/TestQuestions";
import QuestionAnswers from "./testCategoryForm/QuestionAnswers";
import EventUser from "./testCategoryForm/EventUser";

class TestCategoryForm extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onChange", data => {
      this.props.on(data[1], data[0]);
    });

    _defineProperty(this, "onChangeQuestion", data => {
      console.log(data);
    });

    _defineProperty(this, "onRefresh", () => {
      console.log(this.props);
      if (this.props.onRefresh) this.props.onRefresh();
    });

    _defineProperty(this, "onChangeEventUser", () => {
      console.log("data");
    });
  }

  render() {
    switch (this.props.data_type) {
      case "Bio_Test":
        return this.onTest();

      case "Bio_TestQuestion":
        return this.onQuestion();

      case "Bio_Event":
        return this.onEvent();

      default:
        return this.props.defArea;
    }
  }

  onTest() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Switch, null, /*#__PURE__*/React.createElement(Route, {
      exact: true,
      path: this.props.parent_route
    }, /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: "row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-3"
    }), /*#__PURE__*/React.createElement("div", {
      className: "col-md-9"
    }, /*#__PURE__*/React.createElement(Link, {
      to: `${this.props.parent_route}/questions`,
      className: "btn btn-link"
    }, __("edit Questions")))), this.props.defArea)), /*#__PURE__*/React.createElement(Route, {
      exact: true,
      path: `${this.props.parent_route}/questions`
    }, /*#__PURE__*/React.createElement(TestQuestions, _extends({}, this.props, {
      onChange: this.onChange
    })))));
  }

  onQuestion() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Switch, null, /*#__PURE__*/React.createElement(Route, {
      exact: true,
      path: this.props.parent_route
    }, /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: "row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-3"
    }), /*#__PURE__*/React.createElement("div", {
      className: "col-md-9"
    }, /*#__PURE__*/React.createElement(Link, {
      to: `${this.props.parent_route}/answers`,
      className: "btn btn-link"
    }, __("edit Answers")))), this.props.defArea)), /*#__PURE__*/React.createElement(Route, {
      exact: true,
      path: `${this.props.parent_route}/answers`
    }, /*#__PURE__*/React.createElement(QuestionAnswers, _extends({}, this.props, {
      onChange: this.onChangeQuestion
    })))));
  }

  onEvent() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Switch, null, /*#__PURE__*/React.createElement(Route, {
      exact: true,
      path: this.props.parent_route
    }, /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: "row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-3"
    }), /*#__PURE__*/React.createElement("div", {
      className: "col-md-9"
    }, /*#__PURE__*/React.createElement(Link, {
      to: `${this.props.parent_route}/answers`,
      className: "btn btn-link"
    }, __("edit Requesters")))), this.props.defArea)), /*#__PURE__*/React.createElement(Route, {
      exact: true,
      path: `${this.props.parent_route}/answers`
    }, /*#__PURE__*/React.createElement(EventUser, _extends({}, this.props, {
      onChange: this.onChangeEventUser,
      onRefresh: this.onRefresh
    })))));
  }

}

export default compose(withApollo, withRouter)(TestCategoryForm);