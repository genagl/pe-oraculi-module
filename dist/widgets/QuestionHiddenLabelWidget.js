function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import gql from "graphql-tag";
import { __ } from "react-pe-utilities";

class QuestionHiddenLabelWidget extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      col: 0
    });
  }

  componentDidMount() {
    if (this.props.data.data_type == "Bio_TestQuestion") {
      const query = gql`
				query getFullQuestionHiddenCount
				{
					getFullQuestionHiddenCount
				}
			`;
      this.props.client.query({
        query
      }).then(result => {
        // console.log( result );
        this.setState({
          col: result.data.getFullQuestionHiddenCount
        });
      });
    }
  }

  render() {
    // console.log( this.props.data );
    return this.props.data.data_type == "Bio_TestQuestion" ? /*#__PURE__*/React.createElement(React.Fragment, null, this.state.col > 0 ? /*#__PURE__*/React.createElement("div", {
      className: "menu-labla hint hint--left",
      "data-hint": __("Need approve")
    }, this.state.col) : null) : null;
  }

}

export default compose(withApollo)(QuestionHiddenLabelWidget);