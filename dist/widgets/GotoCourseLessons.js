function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import { __ } from "react-pe-utilities";

class GotoCourseLessons extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onAddRequest", () => {
      console.log("on add request");
    });
  }

  render() {
    return this.props.is_member ? /*#__PURE__*/React.createElement(NavLink, {
      className: "btn btn-primary mt-4",
      to: `/course-lessons/${this.props.id}`
    }, __("Continue training")) : this.props.is_closed ? /*#__PURE__*/React.createElement("div", {
      className: "alert alert-danger my-3"
    }, __("Course Leaders closed requests to add online.")) : /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
      className: "btn btn-success btn-block mt-4",
      onClick: this.onAddRequest
    }, __("Send request add to Course")));
  }

}

export default compose(withApollo)(GotoCourseLessons);