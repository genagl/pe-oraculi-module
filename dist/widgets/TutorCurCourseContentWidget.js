import React, { Component } from "react";

class TutorCurContentWidget extends Component {
  render() {
    return this.props.post_content ? /*#__PURE__*/React.createElement("div", {
      className: "",
      dangerouslySetInnerHTML: {
        __html: this.props.post_content
      }
    }) : null;
  }

}

export default TutorCurContentWidget;